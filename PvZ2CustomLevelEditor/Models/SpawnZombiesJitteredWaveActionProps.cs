﻿using Newtonsoft.Json;
using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class SpawnZombiesJitteredWaveActionProps : ObjectData
    {
        private List<int> kDynamicPlantFood;
        public int? AdditionalPlantFood { get; set; }
        public List<int> DynamicPlantFood
        {
            get
            {
                if (kDynamicPlantFood != null)
                {
                    if (this.kDynamicPlantFood.Count < 7)
                    {
                        for (int i = this.kDynamicPlantFood.Count; i < 7; i++)
                        {
                            this.kDynamicPlantFood.Add(0);
                        }
                    }
                    else if (this.kDynamicPlantFood.Count > 7)
                    {
                        this.kDynamicPlantFood.RemoveRange(7, this.kDynamicPlantFood.Count - 7);
                    }
                }
                return this.kDynamicPlantFood;
            }
            set
            {
                this.kDynamicPlantFood = value;
            }
        }
        public List<Zombie> AddToZombiePool { get; set; }
        public List<Zombie> Zombies { get; set; }
        
        public SpawnZombiesJitteredWaveActionProps(int waveNum)
        {
            this.Alias = "Wave" + waveNum;
        }
    }

    public class Zombie
    {
        public int? Row { get; set; }
        [JsonProperty("Type")]
        public string TypeInString
        {
            get
            {
                if (this.Type != null)
                {
                    return EnumUtil.ConvertToString(this.Type) + "@ZombieTypes";
                }
                return null;
            }
        }
        [JsonIgnore()]
        public ZombieType Type { get; set; }
    }
}
