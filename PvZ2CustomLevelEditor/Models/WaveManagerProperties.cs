﻿using Newtonsoft.Json;
using PvZ2CustomLevelEditor.Helpers;
using PvZ2CustomLevelEditor.Models.WaveEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class WaveManagerProperties : ObjectData
    {
        public float? MinNextWaveHealthPercentage { get; set; }
        public float? MaxNextWaveHealthPercentage { get; set; }
        public int FlagWaveInterval { get; set; }
        public int? SpawnColEnd { get; set; }
        public int? SpawnColStart { get; set; }
        public int WaveCount { get; set; }
        public int WaveSpendingPointIncrement { get; set; }
        public int WaveSpendingPoints { get; set; }
        [JsonProperty("Waves")]
        public List<List<string>> WavesInString
        {
            get
            {
                List<List<string>> allWaves = new List<List<string>>();
                for (int i = 1; i <= this.WaveCount; i++)
                {
                    List<string> currentWave = new List<string>();
                    currentWave.Add(String.Format("Wave{0}@CurrentLevel", i));

                    if (this.Waves != null)
                    {
                        List<WaveEventType> waveEventTypes = Waves.Where(w => w.WaveAt == i).Select(w => w.Event).ToList();
                        foreach (WaveEventType waveEventType in waveEventTypes)
                        {
                            currentWave.Add(String.Format("Wave{0}{1}0@CurrentLevel", i, EnumUtil.ConvertToString(waveEventType)));
                        }
                    }
                    allWaves.Add(currentWave);
                }
                return allWaves;
            }
        }
        [JsonIgnore()]
        public List<Wave> Waves { get; set; }

        public WaveManagerProperties()
        {
            this.Alias = "WaveManagerProps";
        }
    }

    public class Wave
    {
        public WaveEventType Event { get; set; }
        public int WaveAt { get; set; }
    }
}
