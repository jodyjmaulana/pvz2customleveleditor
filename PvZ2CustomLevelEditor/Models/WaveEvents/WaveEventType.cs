﻿using System;

namespace PvZ2CustomLevelEditor.Models.WaveEvents
{
    public enum WaveEventType
    {
        ModConveyor,
        StormEvent,
        FrostWindEvent,
        ParachuteRainEvent
    }
}
