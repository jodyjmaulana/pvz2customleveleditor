﻿using Newtonsoft.Json;
using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Helpers;
using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models.WaveEvents
{
    public class ParachuteRainZombieSpawnerProps : ObjectData, IWaveEvent
    {
        [JsonIgnore()]
        public WaveEventType WaveEventType
        {
            get
            {
                return WaveEventType.ParachuteRainEvent;
            }
        }
        public int ColumnEnd { get; set; }
        public int ColumnStart { get; set; }
        public int GroupSize { get; set; }
        public int SpiderCount { get; set; }
        public string SpiderZombieName
        {
            get
            {
                return EnumUtil.ConvertToString(ZombieType.lostcity_lostpilot);
            }
        }
        public int TimeBeforeFullSpawn { get; set; }
        public float TimeBetweenGroup { get; set; }
        public string WaveStartMessage { get; set; }
        public float ZombieFallTime { get; set; }
        
        public ParachuteRainZombieSpawnerProps(int waveNum)
        {
            this.Alias = "Wave" + waveNum + "ParachuteRainEvent0";
            this.ColumnStart = 4;
            this.ColumnEnd = 8;
            this.GroupSize = 1;
            this.SpiderCount = 1;
            this.TimeBeforeFullSpawn = 1;
            this.TimeBetweenGroup = 0.2f;
            this.WaveStartMessage = "[WARNING_PARACHUTERAIN]";
            this.ZombieFallTime = 1.5f;
        }
    }
}
