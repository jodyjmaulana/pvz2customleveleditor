﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Helpers;
using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class SeedBankProperties : ObjectData, IObjectLevelModule
    {
        public List<PresetPlant> PresetPlantList { get; set; }
        [JsonProperty("PlantBlackList")]
        public List<string> PlantBlackListInString
        {
            get
            {
                if (PlantBlackList != null && PlantBlackList.Count > 0)
                {
                    List<string> plants = new List<string>();
                    foreach (PlantType plantType in PlantBlackList)
                    {
                        plants.Add(EnumUtil.ConvertToString(plantType));
                    }
                    return plants;
                }
                return null;
            }
        }
        [JsonIgnore()]
        public List<PlantType> PlantBlackList { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public SeedBankSelectionMethod? SelectionMethod { get; set; }

        public SeedBankProperties()
        {
            this.Alias = "SeedBank";
        }
    }

    public class PresetPlant
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public PlantType PlantType { get; set; }
        public int? Level { get; set; }
    }

    public enum SeedBankSelectionMethod
    {
        chooser,
        preset
    }
}
