﻿using PvZ2CustomLevelEditor.Models.WaveEvents;
using System;
namespace PvZ2CustomLevelEditor.Models.Interfaces
{
    public interface IWaveEvent
    {
        WaveEventType WaveEventType { get; }
    }
}
