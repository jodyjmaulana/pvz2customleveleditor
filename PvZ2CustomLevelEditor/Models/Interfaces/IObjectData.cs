﻿using System;

namespace PvZ2CustomLevelEditor.Models.Interfaces
{
    public interface IObjectData
    {
        string Alias { get; set; }
        string ObjClass { get; }
    }
}
