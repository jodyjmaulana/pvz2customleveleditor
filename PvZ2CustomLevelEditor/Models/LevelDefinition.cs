﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Helpers;
using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class LevelDefinition : ObjectData
    {
        [JsonIgnore()]
        public WorldType WorldType { get; set; }
        public string Description { get; set; }
        public string FirstIntroNarrative { get; set; }
        public string FirstOutroNarrative { get; set; }
        public string FirstRewardParam { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public FirstRewardType? FirstRewardType { get; set; }
        [JsonProperty("GameFeaturesToUnlock")]
        public List<string> GameFeaturesToUnlockInString
        {
            get
            {
                if (GameFeaturesToUnlock != null && GameFeaturesToUnlock.Count > 0)
                {
                    List<string> features = new List<string>();
                    foreach (UnlockableGameFeature gameFeature in GameFeaturesToUnlock)
                    {
                        features.Add(EnumUtil.ConvertToString(gameFeature));
                    }
                    return features;
                }
                return null;
            }
        }
        [JsonIgnore()]
        public List<UnlockableGameFeature> GameFeaturesToUnlock { get; set; }
        public int? LevelNumber { get; set; }
        public bool? ForceToWorldMap { get; set; }
        public string Loot { get; set; }
        public List<string> Modules { get; set; }
        public string MusicType { get; set; }
        public string Name { get; set; }
        public string NormalPresentTable
        {
            get
            {
                return StringHelper.GetPresentTable(this.WorldType, "normal");
            }
        }
        public string ShinyPresentTable
        {
            get
            {
                return StringHelper.GetPresentTable(this.WorldType, "shiny");
            }
        }
        public bool? RepeatPlayForceToWorldMap { get; set; }
        public string StageModule { get; set; }
        public bool? SuppressDynamicTutorial { get; set; }
        // not yet used
        public bool? GameOverDialogShowBrain { get; set; }

        public LevelDefinition(WorldType worldType)
        {
            this.WorldType = worldType;
            this.Description = StringHelper.GetWorldTripDescription(worldType);
            this.Loot = "DefaultLoot@LevelModules";
            this.Name = StringHelper.GetWorldLevelName(worldType);
            this.StageModule = StringHelper.GetWorldStageModule(worldType);
        }
    }
}
