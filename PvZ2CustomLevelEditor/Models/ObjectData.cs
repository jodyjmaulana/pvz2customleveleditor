﻿using Newtonsoft.Json;
using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class ObjectData : IObjectData
    {
        [JsonIgnore()]
        public string Alias { get; set; }
        [JsonIgnore()]
        public string ObjClass
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}
