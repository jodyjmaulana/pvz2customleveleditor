﻿using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    class GravestoneProperties : ObjectData, IObjectLevelModule
    {
        public List<GridItem> ForceSpawnData { get; set; }
        public int? GravestoneCount { get; set; }
        public int? SpawnColumnEnd { get; set; }
        public int? SpawnColumnStart { get; set; }

        public GravestoneProperties()
        {
            this.Alias = "Gravestones";
        }
    }
}
