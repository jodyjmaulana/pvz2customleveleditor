﻿using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class SunDropperProperties : ObjectData, IObjectLevelModule
    {
        public int InitialSunDropDelay { get; set; }
        public int SunCountdownBase { get; set; }
        public int SunCountdownIncreasePerSun { get; set; }
        public int SunCountdownMax { get; set; }
        public int SunCountdownRange { get; set; }

        public SunDropperProperties()
        {
            this.Alias = "DefaultSunDropper";
            this.InitialSunDropDelay = 2;
            this.SunCountdownBase = 4;
            this.SunCountdownIncreasePerSun = 0;
            this.SunCountdownMax = 4;
            this.SunCountdownRange = 0;
        }
    }
}
