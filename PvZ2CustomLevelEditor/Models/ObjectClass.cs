﻿using Newtonsoft.Json;
using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class ObjectClass
    {
        private List<string> kAliases;
        [JsonProperty("aliases")]
        public List<string> Aliases
        {
            get
            {
                if (this.ObjData != null && !String.IsNullOrWhiteSpace(this.ObjData.Alias))
                {
                    this.kAliases = new List<string>();
                    this.kAliases.Add(this.ObjData.Alias);
                    return this.kAliases;
                }
                return null;
            }
        }
        [JsonProperty("objclass")]
        public string ObjClass
        {
            get
            {
                if (this.ObjData != null)
                {
                    return this.ObjData.ObjClass;
                }
                return String.Empty;
            }
        }
        [JsonProperty("objdata")]
        public IObjectData ObjData { get; set; }
    }
}
