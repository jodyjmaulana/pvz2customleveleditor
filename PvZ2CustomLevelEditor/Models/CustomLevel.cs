﻿using Newtonsoft.Json;
using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class CustomLevel
    {
        [JsonProperty("#comment")]
        public string Comment { get; set; }
        [JsonProperty("objects")]
        public List<ObjectClass> Objects { get; set; }
        [JsonProperty("version")]
        public int Version { get; set; }

        public CustomLevel()
        {
            this.Comment = "none";
            this.Objects = new List<ObjectClass>(); 
            this.Version = 1;
        }

        public void UpdateLevelDefinitionModules() {
            var levelDefinition = this.Objects.Where(n => n.ObjData is LevelDefinition).FirstOrDefault();
            if (levelDefinition != null)
            {
                foreach (ObjectClass objectClass in this.Objects)
                {
                    if (objectClass.ObjData is IObjectLevelModule)
                    {
                        (levelDefinition.ObjData as LevelDefinition).Modules.Add(objectClass.Aliases.FirstOrDefault() + "@CurrentLevel");
                        if (objectClass.ObjData is SunDropperProperties)
                        {
                            (levelDefinition.ObjData as LevelDefinition).Modules.Remove("DefaultSunDropper@LevelModules");
                        }
                    }
                }
            }
        }
    }
}
