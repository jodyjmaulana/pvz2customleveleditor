﻿using Newtonsoft.Json;
using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Helpers;
using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class WaveManagerModuleProperties : ObjectData, IObjectLevelModule
    {
        public List<DynamicZombie> DynamicZombies { get; set; }
        public string WaveManagerProps
        {
            get
            {
                return "WaveManagerProps@CurrentLevel";
            }
        }
        public WaveManagerModuleProperties()
        {
            this.Alias = "NewWaves";
        }
    }

    public class DynamicZombie
    {
        public int PointIncrementPerWave { get; set; }
        public int StartingPoints { get; set; }
        public int StartingWave { get; set; }
        [JsonProperty("ZombiePool")]
        public List<string> ZombiePoolInString
        {
            get
            {
                if (ZombiePool != null)
                {
                    var zombie = ZombiePool.Select(z => EnumUtil.ConvertToString(z) + "@ZombieTypes").ToList();
                    return zombie;
                }
                return null;
            }
        }
        [JsonIgnore()]
        public List<ZombieType> ZombiePool { get; set; }
    }
}
