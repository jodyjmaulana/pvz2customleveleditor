﻿using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    class InitialZombieProperties : ObjectData, IObjectLevelModule
    {
        public List<GridItem> InitialZombiePlacements { get; set; }

        public InitialZombieProperties()
        {
            this.Alias = "FrozenZombiePlacement";
        }
    }
}
