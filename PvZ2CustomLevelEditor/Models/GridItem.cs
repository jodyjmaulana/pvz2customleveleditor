﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class GridItem
    {
        public string Condition
        {
            get
            {
                if (this.IsFrozen)
                {
                    return "icecubed";
                }
                return null;
            }
        }
        [JsonIgnore()]
        public bool IsFrozen { get; set; }
        public int GridX { get; set; }
        public int GridY { get; set; }
        public string TypeName { get; set; }

        public GridItem()
        {
            this.IsFrozen = false;
        }
    }
}
