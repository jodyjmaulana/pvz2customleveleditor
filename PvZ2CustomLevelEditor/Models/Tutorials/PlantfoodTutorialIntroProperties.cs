﻿using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models.Tutorials
{
    public class PlantfoodTutorialIntroProperties : BaseTutorialIntro
    {
        public PlantfoodTutorialIntroProperties()
        {
            this.Alias = "PlantfoodTutorial";
        }
    }
}
