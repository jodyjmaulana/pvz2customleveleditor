﻿using PvZ2CustomLevelEditor.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Models
{
    public class InitialPlantProperties : ObjectData, IObjectLevelModule
    {
        public List<GridItem> InitialPlantPlacements { get; set; }

        public InitialPlantProperties()
        {
            this.Alias = "InitialPlantPlacement";
        }
    }
}
