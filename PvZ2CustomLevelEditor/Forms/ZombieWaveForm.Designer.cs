﻿namespace PvZ2CustomLevelEditor.Forms
{
    partial class ZombieWaveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.acceptButton = new System.Windows.Forms.Button();
            this.zombieInWaveLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // acceptButton
            // 
            this.acceptButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.acceptButton.Location = new System.Drawing.Point(690, 412);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 2;
            this.acceptButton.Text = "OK";
            this.acceptButton.UseVisualStyleBackColor = true;
            // 
            // zombieInWaveLayoutPanel
            // 
            this.zombieInWaveLayoutPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zombieInWaveLayoutPanel.Location = new System.Drawing.Point(12, 12);
            this.zombieInWaveLayoutPanel.Name = "zombieInWaveLayoutPanel";
            this.zombieInWaveLayoutPanel.Size = new System.Drawing.Size(422, 146);
            this.zombieInWaveLayoutPanel.TabIndex = 3;
            // 
            // ZombieWaveForm
            // 
            this.AcceptButton = this.acceptButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 447);
            this.ControlBox = false;
            this.Controls.Add(this.zombieInWaveLayoutPanel);
            this.Controls.Add(this.acceptButton);
            this.Name = "ZombieWaveForm";
            this.Text = "ZombieWaveForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button acceptButton;
        public System.Windows.Forms.FlowLayoutPanel zombieInWaveLayoutPanel;
    }
}