﻿using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Forms.Components;
using PvZ2CustomLevelEditor.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvZ2CustomLevelEditor.Forms
{
    public partial class ZombieWaveForm : Form
    {
        public ZombieListTablePanel zombieListTablePanel;
        private int Ordinal { get; set; }

        public ZombieWaveForm()
        {
            InitializeComponent();
            this.zombieListTablePanel = new ZombieListTablePanel();
            this.zombieListTablePanel.ZombieInWaveLayoutPanel = zombieInWaveLayoutPanel;
            this.Controls.Add(this.zombieListTablePanel);
            this.zombieListTablePanel.zombieRowGroupBox.ResumeLayout(false);
            this.zombieListTablePanel.zombieRowGroupBox.PerformLayout();
            this.zombieListTablePanel.ResumeLayout(false);
        }
    }
}
