﻿namespace PvZ2CustomLevelEditor.Forms
{
    partial class SeedBankForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.acceptButton = new System.Windows.Forms.Button();
            this.plantListLayoutPanelDummy = new System.Windows.Forms.FlowLayoutPanel();
            this.seedBankGroupBox = new System.Windows.Forms.GroupBox();
            this.warningLabel = new System.Windows.Forms.Label();
            this.blacklistedPlantsLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.presetPlantsLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.blacklistedPlantsRadioButton = new System.Windows.Forms.RadioButton();
            this.presetPlantsRadioButton = new System.Windows.Forms.RadioButton();
            this.seedSelectionTypeGroup = new System.Windows.Forms.GroupBox();
            this.presetRadioButton = new System.Windows.Forms.RadioButton();
            this.chooserRadioButton = new System.Windows.Forms.RadioButton();
            this.plantLevelGroupBox = new System.Windows.Forms.GroupBox();
            this.levelNumBox = new System.Windows.Forms.NumericUpDown();
            this.setLevelCheckBox = new System.Windows.Forms.CheckBox();
            this.seedBankGroupBox.SuspendLayout();
            this.seedSelectionTypeGroup.SuspendLayout();
            this.plantLevelGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.levelNumBox)).BeginInit();
            this.SuspendLayout();
            // 
            // acceptButton
            // 
            this.acceptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.acceptButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.acceptButton.Location = new System.Drawing.Point(572, 386);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 0;
            this.acceptButton.Text = "OK";
            this.acceptButton.UseVisualStyleBackColor = true;
            // 
            // plantListLayoutPanelDummy
            // 
            this.plantListLayoutPanelDummy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plantListLayoutPanelDummy.AutoScroll = true;
            this.plantListLayoutPanelDummy.Location = new System.Drawing.Point(345, 64);
            this.plantListLayoutPanelDummy.Name = "plantListLayoutPanelDummy";
            this.plantListLayoutPanelDummy.Size = new System.Drawing.Size(302, 316);
            this.plantListLayoutPanelDummy.TabIndex = 1;
            this.plantListLayoutPanelDummy.Visible = false;
            this.plantListLayoutPanelDummy.MouseEnter += new System.EventHandler(this.panel_onMouseEnter);
            // 
            // seedBankGroupBox
            // 
            this.seedBankGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.seedBankGroupBox.Controls.Add(this.warningLabel);
            this.seedBankGroupBox.Controls.Add(this.blacklistedPlantsLayoutPanel);
            this.seedBankGroupBox.Controls.Add(this.presetPlantsLayoutPanel);
            this.seedBankGroupBox.Controls.Add(this.blacklistedPlantsRadioButton);
            this.seedBankGroupBox.Controls.Add(this.presetPlantsRadioButton);
            this.seedBankGroupBox.Location = new System.Drawing.Point(12, 64);
            this.seedBankGroupBox.Name = "seedBankGroupBox";
            this.seedBankGroupBox.Size = new System.Drawing.Size(327, 316);
            this.seedBankGroupBox.TabIndex = 2;
            this.seedBankGroupBox.TabStop = false;
            // 
            // warningLabel
            // 
            this.warningLabel.AutoSize = true;
            this.warningLabel.ForeColor = System.Drawing.Color.Red;
            this.warningLabel.Location = new System.Drawing.Point(7, 297);
            this.warningLabel.Name = "warningLabel";
            this.warningLabel.Size = new System.Drawing.Size(0, 13);
            this.warningLabel.TabIndex = 4;
            // 
            // blacklistedPlantsLayoutPanel
            // 
            this.blacklistedPlantsLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.blacklistedPlantsLayoutPanel.AutoScroll = true;
            this.blacklistedPlantsLayoutPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.blacklistedPlantsLayoutPanel.Location = new System.Drawing.Point(6, 158);
            this.blacklistedPlantsLayoutPanel.Name = "blacklistedPlantsLayoutPanel";
            this.blacklistedPlantsLayoutPanel.Size = new System.Drawing.Size(315, 132);
            this.blacklistedPlantsLayoutPanel.TabIndex = 3;
            this.blacklistedPlantsLayoutPanel.MouseEnter += new System.EventHandler(this.panel_onMouseEnter);
            // 
            // presetPlantsLayoutPanel
            // 
            this.presetPlantsLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.presetPlantsLayoutPanel.AutoScroll = true;
            this.presetPlantsLayoutPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.presetPlantsLayoutPanel.Location = new System.Drawing.Point(6, 42);
            this.presetPlantsLayoutPanel.Name = "presetPlantsLayoutPanel";
            this.presetPlantsLayoutPanel.Size = new System.Drawing.Size(315, 87);
            this.presetPlantsLayoutPanel.TabIndex = 2;
            this.presetPlantsLayoutPanel.MouseEnter += new System.EventHandler(this.panel_onMouseEnter);
            // 
            // blacklistedPlantsRadioButton
            // 
            this.blacklistedPlantsRadioButton.AutoSize = true;
            this.blacklistedPlantsRadioButton.Location = new System.Drawing.Point(6, 135);
            this.blacklistedPlantsRadioButton.Name = "blacklistedPlantsRadioButton";
            this.blacklistedPlantsRadioButton.Size = new System.Drawing.Size(108, 17);
            this.blacklistedPlantsRadioButton.TabIndex = 1;
            this.blacklistedPlantsRadioButton.Text = "Blacklisted Plants";
            this.blacklistedPlantsRadioButton.UseVisualStyleBackColor = true;
            // 
            // presetPlantsRadioButton
            // 
            this.presetPlantsRadioButton.AutoSize = true;
            this.presetPlantsRadioButton.Checked = true;
            this.presetPlantsRadioButton.Location = new System.Drawing.Point(6, 19);
            this.presetPlantsRadioButton.Name = "presetPlantsRadioButton";
            this.presetPlantsRadioButton.Size = new System.Drawing.Size(87, 17);
            this.presetPlantsRadioButton.TabIndex = 0;
            this.presetPlantsRadioButton.TabStop = true;
            this.presetPlantsRadioButton.Text = "Preset Plants";
            this.presetPlantsRadioButton.UseVisualStyleBackColor = true;
            // 
            // seedSelectionTypeGroup
            // 
            this.seedSelectionTypeGroup.Controls.Add(this.presetRadioButton);
            this.seedSelectionTypeGroup.Controls.Add(this.chooserRadioButton);
            this.seedSelectionTypeGroup.Location = new System.Drawing.Point(12, 12);
            this.seedSelectionTypeGroup.Name = "seedSelectionTypeGroup";
            this.seedSelectionTypeGroup.Size = new System.Drawing.Size(327, 46);
            this.seedSelectionTypeGroup.TabIndex = 3;
            this.seedSelectionTypeGroup.TabStop = false;
            this.seedSelectionTypeGroup.Text = "Seed Selection Type";
            // 
            // presetRadioButton
            // 
            this.presetRadioButton.AutoSize = true;
            this.presetRadioButton.Location = new System.Drawing.Point(77, 19);
            this.presetRadioButton.Name = "presetRadioButton";
            this.presetRadioButton.Size = new System.Drawing.Size(55, 17);
            this.presetRadioButton.TabIndex = 1;
            this.presetRadioButton.Text = "Preset";
            this.presetRadioButton.UseVisualStyleBackColor = true;
            // 
            // chooserRadioButton
            // 
            this.chooserRadioButton.AutoSize = true;
            this.chooserRadioButton.Checked = true;
            this.chooserRadioButton.Location = new System.Drawing.Point(6, 19);
            this.chooserRadioButton.Name = "chooserRadioButton";
            this.chooserRadioButton.Size = new System.Drawing.Size(64, 17);
            this.chooserRadioButton.TabIndex = 0;
            this.chooserRadioButton.TabStop = true;
            this.chooserRadioButton.Text = "Chooser";
            this.chooserRadioButton.UseVisualStyleBackColor = true;
            // 
            // plantLevelGroupBox
            // 
            this.plantLevelGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plantLevelGroupBox.Controls.Add(this.levelNumBox);
            this.plantLevelGroupBox.Controls.Add(this.setLevelCheckBox);
            this.plantLevelGroupBox.Location = new System.Drawing.Point(346, 12);
            this.plantLevelGroupBox.Name = "plantLevelGroupBox";
            this.plantLevelGroupBox.Size = new System.Drawing.Size(301, 46);
            this.plantLevelGroupBox.TabIndex = 4;
            this.plantLevelGroupBox.TabStop = false;
            this.plantLevelGroupBox.Text = "Plant Level";
            // 
            // levelNumBox
            // 
            this.levelNumBox.Enabled = false;
            this.levelNumBox.Location = new System.Drawing.Point(112, 19);
            this.levelNumBox.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.levelNumBox.Name = "levelNumBox";
            this.levelNumBox.Size = new System.Drawing.Size(120, 20);
            this.levelNumBox.TabIndex = 1;
            // 
            // setLevelCheckBox
            // 
            this.setLevelCheckBox.AutoSize = true;
            this.setLevelCheckBox.Location = new System.Drawing.Point(7, 20);
            this.setLevelCheckBox.Name = "setLevelCheckBox";
            this.setLevelCheckBox.Size = new System.Drawing.Size(98, 17);
            this.setLevelCheckBox.TabIndex = 0;
            this.setLevelCheckBox.Text = "Set Plant Level";
            this.setLevelCheckBox.UseVisualStyleBackColor = true;
            this.setLevelCheckBox.CheckedChanged += new System.EventHandler(this.setLevelCheckBox_onCheckedChanged);
            // 
            // SeedBankForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 421);
            this.ControlBox = false;
            this.Controls.Add(this.plantLevelGroupBox);
            this.Controls.Add(this.seedSelectionTypeGroup);
            this.Controls.Add(this.seedBankGroupBox);
            this.Controls.Add(this.plantListLayoutPanelDummy);
            this.Controls.Add(this.acceptButton);
            this.Name = "SeedBankForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Seed Bank Properties";
            this.seedBankGroupBox.ResumeLayout(false);
            this.seedBankGroupBox.PerformLayout();
            this.seedSelectionTypeGroup.ResumeLayout(false);
            this.seedSelectionTypeGroup.PerformLayout();
            this.plantLevelGroupBox.ResumeLayout(false);
            this.plantLevelGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.levelNumBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.FlowLayoutPanel plantListLayoutPanelDummy;
        private System.Windows.Forms.GroupBox seedBankGroupBox;
        private System.Windows.Forms.RadioButton blacklistedPlantsRadioButton;
        private System.Windows.Forms.RadioButton presetPlantsRadioButton;
        private System.Windows.Forms.Label warningLabel;
        public System.Windows.Forms.FlowLayoutPanel blacklistedPlantsLayoutPanel;
        public System.Windows.Forms.FlowLayoutPanel presetPlantsLayoutPanel;
        private System.Windows.Forms.GroupBox seedSelectionTypeGroup;
        public System.Windows.Forms.RadioButton chooserRadioButton;
        public System.Windows.Forms.RadioButton presetRadioButton;
        private System.Windows.Forms.GroupBox plantLevelGroupBox;
        private System.Windows.Forms.NumericUpDown levelNumBox;
        private System.Windows.Forms.CheckBox setLevelCheckBox;
    }
}