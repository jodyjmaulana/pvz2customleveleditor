﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvZ2CustomLevelEditor.Forms
{
    public partial class MiniGameForm : Form
    {
        public MiniGameForm()
        {
            InitializeComponent();
        }

        private void MiniGameForm_onLoad(object sender, EventArgs e)
        {
            if (!sunBombCheckBox.Enabled)
            {
                sunBombCheckBox.Checked = false;
            }
        }

    }
}
