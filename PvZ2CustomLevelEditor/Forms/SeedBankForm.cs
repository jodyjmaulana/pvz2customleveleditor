﻿using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Forms.Components;
using PvZ2CustomLevelEditor.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvZ2CustomLevelEditor.Forms
{
    public partial class SeedBankForm : Form
    {
        private PlantListLayoutPanel plantListLayoutPanel;

        public SeedBankForm()
        {
            InitializeComponent();
            this.plantListLayoutPanel = new PlantListLayoutPanel(plantListLayoutPanelDummy);
            this.Controls.Add(this.plantListLayoutPanel);

            this.AddHandlerToPlantListLayoutPanelButtons();
        }

        private void AddHandlerToPlantListLayoutPanelButtons()
        {
            var plantButtons = this.plantListLayoutPanel.Controls.OfType<Button>().ToList();
            foreach (var plantButton in plantButtons)
            {
                plantButton.Click += new System.EventHandler(this.plantListButton_onClick);
            }
        }

        private void plantListButton_onClick(object sender, EventArgs e)
        {
            warningLabel.Text = "";
            string plant = ((Control)(sender)).Name.Split(new char[] { '-' }, 2)[1];
            if (presetPlantsRadioButton.Checked)
            {
                if (presetPlantsLayoutPanel.Controls.OfType<Button>().Count() < 8)
                {
                    int level = 0;
                    if (setLevelCheckBox.Checked)
                    {
                        level = (int)levelNumBox.Value;
                    }

                    Button button = new Button();
                    button.Name = String.Format("preset-{0}-{1}", plant, level);
                    button.Image = ImageHelper.GetPlantThumbnail(plant);
                    button.Size = new Size(40, 40);
                    button.Margin = new System.Windows.Forms.Padding(0);
                    button.Click += new System.EventHandler(this.selectedPlantsButton_onClick);
                    presetPlantsLayoutPanel.Controls.Add(button);

                    Label label = new Label();
                    label.Name = String.Format("selected-{0}", plant);
                    label.Text = level.ToString();
                    label.Size = new Size(12, 12);
                    label.Margin = new System.Windows.Forms.Padding(0);
                    presetPlantsLayoutPanel.Controls.Add(label);
                }
                else
                {
                    warningLabel.Text = "Maximum preset plant number is 8.";
                }
            }
            else if (blacklistedPlantsRadioButton.Checked)
            {
                if (blacklistedPlantsLayoutPanel.Controls.OfType<Button>().FirstOrDefault(x => x.Name.Equals("blacklist-" + plant)) == null)
                {
                    Button button = new Button();
                    button.Name = String.Format("blacklist-{0}", plant);
                    button.Image = ImageHelper.GetPlantThumbnail(plant);
                    button.Size = new Size(40, 40);
                    button.Margin = new System.Windows.Forms.Padding(0);
                    button.Click += new System.EventHandler(this.selectedPlantsButton_onClick);
                    blacklistedPlantsLayoutPanel.Controls.Add(button);
                }
                else
                {
                    warningLabel.Text = "Plant is already selected.";
                }
            }
        }

        private void selectedPlantsButton_onClick(object sender, EventArgs e)
        {
            string labelName = ((Control)(sender)).Name.Split(new char[] { '-' })[0];
            if (labelName.Equals("preset"))
            {
                presetPlantsLayoutPanel.Controls.Remove((Button)(sender));

                string plant = ((Control)(sender)).Name.Split(new char[] { '-' })[1];
                Label label = this.presetPlantsLayoutPanel.Controls.OfType<Label>().FirstOrDefault(n => n.Name.Contains(plant));
                this.presetPlantsLayoutPanel.Controls.Remove(label);
            }
            else if (labelName.Equals("blacklist"))
            {
                blacklistedPlantsLayoutPanel.Controls.Remove((Button)(sender));
            }
        }

        private void setLevelCheckBox_onCheckedChanged(object sender, EventArgs e)
        {
            if (setLevelCheckBox.Checked)
            {
                levelNumBox.Enabled = true;
            }
            else
            {
                levelNumBox.Enabled = false;
            }
        }

        private void panel_onMouseEnter(object sender, EventArgs e)
        {
            ((Control)(sender)).Focus();
        }
    }
}
