﻿using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvZ2CustomLevelEditor.Forms.Components
{
    public class ZombieListTablePanel : TableLayoutPanel
    {
        public FlowLayoutPanel zombieListLayoutPanel;
        public GroupBox zombieRowGroupBox;
        public RadioButton row5RadioButton;
        public RadioButton row4RadioButton;
        public RadioButton row3RadioButton;
        public RadioButton row2RadioButton;
        public RadioButton row1RadioButton;
        public RadioButton row0RadioButton;
        public FlowLayoutPanel ZombieInWaveLayoutPanel { get; set; }
        private List<ZombieType> RegularZombies { get; set; }
        private List<ZombieType> UniqueZombies { get; set; }
        private List<ZombieType> SpecialZombies { get; set; }
        private List<ZombieType> ZombotZombies { get; set; }
        private int Ordinal { get; set; }

        public ZombieListTablePanel()
        {
            this.InitializeComponents();
            this.Ordinal = 1;
            this.InitializeZombieList();
            this.PopulateZombieList();
        }

        private void InitializeComponents()
        {
            this.row5RadioButton = new RadioButton();
            this.row4RadioButton = new RadioButton();
            this.row3RadioButton = new RadioButton();
            this.row2RadioButton = new RadioButton();
            this.row1RadioButton = new RadioButton();
            this.row0RadioButton = new RadioButton();
            this.zombieRowGroupBox = new GroupBox();
            this.zombieListLayoutPanel = new FlowLayoutPanel();
            this.zombieRowGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // row5RadioButton
            // 
            this.row5RadioButton.AutoSize = true;
            this.row5RadioButton.Location = new System.Drawing.Point(211, 19);
            this.row5RadioButton.Name = "row5RadioButton";
            this.row5RadioButton.Size = new System.Drawing.Size(31, 17);
            this.row5RadioButton.TabIndex = 5;
            this.row5RadioButton.Text = "5";
            this.row5RadioButton.UseVisualStyleBackColor = true;
            // 
            // row4RadioButton
            // 
            this.row4RadioButton.AutoSize = true;
            this.row4RadioButton.Location = new System.Drawing.Point(174, 19);
            this.row4RadioButton.Name = "row4RadioButton";
            this.row4RadioButton.Size = new System.Drawing.Size(31, 17);
            this.row4RadioButton.TabIndex = 4;
            this.row4RadioButton.Text = "4";
            this.row4RadioButton.UseVisualStyleBackColor = true;
            // 
            // row3RadioButton
            // 
            this.row3RadioButton.AutoSize = true;
            this.row3RadioButton.Location = new System.Drawing.Point(137, 19);
            this.row3RadioButton.Name = "row3RadioButton";
            this.row3RadioButton.Size = new System.Drawing.Size(31, 17);
            this.row3RadioButton.TabIndex = 3;
            this.row3RadioButton.Text = "3";
            this.row3RadioButton.UseVisualStyleBackColor = true;
            // 
            // row2RadioButton
            // 
            this.row2RadioButton.AutoSize = true;
            this.row2RadioButton.Location = new System.Drawing.Point(100, 19);
            this.row2RadioButton.Name = "row2RadioButton";
            this.row2RadioButton.Size = new System.Drawing.Size(31, 17);
            this.row2RadioButton.TabIndex = 2;
            this.row2RadioButton.Text = "2";
            this.row2RadioButton.UseVisualStyleBackColor = true;
            // 
            // row1RadioButton
            // 
            this.row1RadioButton.AutoSize = true;
            this.row1RadioButton.Location = new System.Drawing.Point(63, 19);
            this.row1RadioButton.Name = "row1RadioButton";
            this.row1RadioButton.Size = new System.Drawing.Size(31, 17);
            this.row1RadioButton.TabIndex = 1;
            this.row1RadioButton.Text = "1";
            this.row1RadioButton.UseVisualStyleBackColor = true;
            // 
            // row0RadioButton
            // 
            this.row0RadioButton.AutoSize = true;
            this.row0RadioButton.Checked = true;
            this.row0RadioButton.Location = new System.Drawing.Point(6, 19);
            this.row0RadioButton.Name = "row0RadioButton";
            this.row0RadioButton.Size = new System.Drawing.Size(51, 17);
            this.row0RadioButton.TabIndex = 0;
            this.row0RadioButton.TabStop = true;
            this.row0RadioButton.Text = "None";
            this.row0RadioButton.UseVisualStyleBackColor = true;
            // 
            // zombieRowGroupBox
            // 
            this.zombieRowGroupBox.Controls.Add(this.row5RadioButton);
            this.zombieRowGroupBox.Controls.Add(this.row4RadioButton);
            this.zombieRowGroupBox.Controls.Add(this.row3RadioButton);
            this.zombieRowGroupBox.Controls.Add(this.row2RadioButton);
            this.zombieRowGroupBox.Controls.Add(this.row1RadioButton);
            this.zombieRowGroupBox.Controls.Add(this.row0RadioButton);
            this.zombieRowGroupBox.Location = new System.Drawing.Point(3, 3);
            this.zombieRowGroupBox.Name = "zombieRowGroupBox";
            this.zombieRowGroupBox.Size = new System.Drawing.Size(319, 39);
            this.zombieRowGroupBox.TabIndex = 1;
            this.zombieRowGroupBox.TabStop = false;
            this.zombieRowGroupBox.Text = "Zombie Row";
            // 
            // zombieListLayoutPanel
            // 
            this.zombieListLayoutPanel.AutoScroll = true;
            this.zombieListLayoutPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.zombieListLayoutPanel.Location = new System.Drawing.Point(3, 48);
            this.zombieListLayoutPanel.Name = "zombieListLayoutPanel";
            this.zombieListLayoutPanel.Size = new System.Drawing.Size(317, 306);
            this.zombieListLayoutPanel.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.ColumnCount = 1;
            this.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Controls.Add(this.zombieListLayoutPanel, 0, 1);
            this.Controls.Add(this.zombieRowGroupBox, 0, 0);
            this.Location = new System.Drawing.Point(440, 12);
            this.Name = "zombieListTablePanel";
            this.RowCount = 2;
            this.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Size = new System.Drawing.Size(325, 357);
            this.TabIndex = 4;
        }

        private void InitializeZombieList()
        {
            this.RegularZombies = new List<ZombieType>()
            {
                ZombieType.mummy,
                ZombieType.mummy_armor1,
                ZombieType.mummy_armor2,
                ZombieType.mummy_armor4,
                ZombieType.egypt_imp,
                ZombieType.egypt_gargantuar,
                ZombieType.pirate,
                ZombieType.pirate_armor1,
                ZombieType.pirate_armor2,
                ZombieType.pirate_armor4,
                ZombieType.pirate_imp,
                ZombieType.pirate_gargantuar,
                ZombieType.cowboy,
                ZombieType.cowboy_armor1,
                ZombieType.cowboy_armor2,
                ZombieType.west_bullrider,
                ZombieType.cowboy_gargantuar,
                ZombieType.future,
                ZombieType.future_armor1,
                ZombieType.future_armor2,
                ZombieType.future_armor4,
                ZombieType.future_imp,
                ZombieType.future_gargantuar,
                ZombieType.dark,
                ZombieType.dark_armor1,
                ZombieType.dark_armor2,
                ZombieType.dark_armor3,
                ZombieType.dark_imp,
                ZombieType.dark_gargantuar,
                ZombieType.beach,
                ZombieType.beach_armor1,
                ZombieType.beach_armor2,
                ZombieType.beach_fem,
                ZombieType.beach_fem_armor1,
                ZombieType.beach_fem_armor2,
                ZombieType.beach_imp,
                ZombieType.beach_gargantuar,
                ZombieType.iceage,
                ZombieType.iceage_armor1,
                ZombieType.iceage_armor2,
                ZombieType.iceage_armor3,
                ZombieType.iceage_imp,
                ZombieType.iceage_gargantuar,
                ZombieType.lostcity,
                ZombieType.lostcity_armor1,
                ZombieType.lostcity_armor2,
                ZombieType.lostcity_imp,
                ZombieType.lostcity_gargantuar,
                ZombieType.eighties,
                ZombieType.eighties_armor1,
                ZombieType.eighties_armor2,
                ZombieType.eighties_imp,
                ZombieType.eighties_gargantuar,
                ZombieType.dino,
                ZombieType.dino_armor1,
                ZombieType.dino_armor2,
                ZombieType.dino_armor3,
                ZombieType.dino_armor4,
                ZombieType.dino_imp,
                ZombieType.dino_gargantuar,
                ZombieType.tutorial,
                ZombieType.tutorial_armor1,
                ZombieType.tutorial_armor2,
                ZombieType.tutorial_armor4,
                ZombieType.tutorial_imp,
                ZombieType.tutorial_gargantuar
            };
            this.UniqueZombies = new List<ZombieType>()
            {
                ZombieType.ra,
                //ZombieType.camel_almanac,
                //ZombieType.camel_onehump,
                //ZombieType.camel_twohump,
                ZombieType.camel_manyhump,
                ZombieType.explorer,
                ZombieType.explorer_veteran,
                ZombieType.tomb_raiser,
                ZombieType.pharaoh,
                ZombieType.swashbuckler,
                ZombieType.seagull,
                ZombieType.pelican,
                ZombieType.barrelroller,
                ZombieType.cannon,
                ZombieType.pirate_captain,
                ZombieType.prospector,
                ZombieType.piano,
                ZombieType.poncho,
                ZombieType.poncho_plate,
                ZombieType.poncho_no_plate,
                ZombieType.chicken_farmer,
                ZombieType.west_bull,
                ZombieType.future_jetpack,
                ZombieType.future_jetpack_veteran,
                ZombieType.future_protector,
                ZombieType.mech_cone,
                ZombieType.disco_mech,
                ZombieType.football_mech,
                ZombieType.dark_juggler,
                ZombieType.dark_wizard,
                ZombieType.dark_king,
                ZombieType.dark_imp_dragon,
                ZombieType.beach_snorkel,
                ZombieType.beach_surfer,
                ZombieType.beach_fisherman,
                ZombieType.beach_octopus,
                ZombieType.iceage_hunter,
                ZombieType.iceage_dodo,
                ZombieType.iceage_weaselhoarder,
                ZombieType.iceage_troglobite,
                ZombieType.iceage_troglobite_2block,
                ZombieType.iceage_troglobite_1block,
                ZombieType.lostcity_excavator,
                ZombieType.lostcity_jane,
                ZombieType.lostcity_bug,
                ZombieType.lostcity_bug_armor1,
                ZombieType.lostcity_bug_armor2,
                ZombieType.lostcity_impporter,
                ZombieType.lostcity_crystalskull,
                ZombieType.lostcity_relichunter,
                ZombieType.eighties_punk,
                ZombieType.eighties_mc,
                ZombieType.eighties_glitter,
                ZombieType.eighties_breakdancer,
                ZombieType.eighties_arcade,
                ZombieType.eighties_boombox,
                ZombieType.dino_bully,
                ZombieType.dino_bully_veteran,
                ZombieType.modern_newspaper,
                ZombieType.modern_newspaper_veteran,
                ZombieType.modern_balloon,
                ZombieType.modern_allstar,
                ZombieType.modern_superfanimp
            };
            this.SpecialZombies = new List<ZombieType>()
            {
                ZombieType.mummy_flag,
                ZombieType.mummy_flag_veteran,
                ZombieType.pirate_flag,
                ZombieType.pirate_flag_veteran,
                ZombieType.cowboy_flag,
                ZombieType.future_flag,
                ZombieType.future_flag_veteran,
                ZombieType.dark_flag,
                ZombieType.beach_flag,
                ZombieType.iceage_flag,
                ZombieType.lostcity_flag,
                ZombieType.eighties_flag,
                ZombieType.dino_flag,
                ZombieType.dino_flag_veteran,
                ZombieType.tutorial_flag,
                ZombieType.tutorial_flag_veteran,
                ZombieType.pirate_captain_parrot,
                ZombieType.chicken,
                ZombieType.future_jetpack_disco,
                ZombieType.iceage_weasel,
                ZombieType.lostcity_lostpilot,
                ZombieType.eighties_8bit,
                ZombieType.eighties_8bit_armor1,
                ZombieType.eighties_8bit_armor2,
                ZombieType.vase_gargantuar,
                ZombieType.treasureyeti
            };
            this.ZombotZombies = new List<ZombieType>()
            {
                ZombieType.zombossmech_egypt,
                ZombieType.zombossmech_pirate,
                ZombieType.zombossmech_cowboy,
                ZombieType.zombossmech_future,
                ZombieType.zombossmech_dark,
                ZombieType.zombossmech_beach,
                ZombieType.zombossmech_iceage,
                ZombieType.zombossmech_lostcity,
                ZombieType.zombossmech_eighties,
                ZombieType.zombossmech_dino
            };
        }

        private void PopulateZombieList()
        {
            Label label = new Label();
            label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label.Text = "Regular zombies";
            label.TextAlign = ContentAlignment.BottomLeft;
            this.zombieListLayoutPanel.Controls.Add(label);
            this.zombieListLayoutPanel.SetFlowBreak(this.zombieListLayoutPanel.Controls[this.zombieListLayoutPanel.Controls.Count - 1], true);
            foreach (ZombieType zombie in this.RegularZombies)
            {
                Button button = new Button();
                button.Name = "button-" + EnumUtil.ConvertToString(zombie);
                button.Size = new Size(40, 40);
                button.Margin = new System.Windows.Forms.Padding(0);
                button.Image = ImageHelper.GetZombieThumbnail(zombie);
                button.Click += new System.EventHandler(this.zombieListButton_onClick);
                this.zombieListLayoutPanel.Controls.Add(button);
            }

            this.zombieListLayoutPanel.SetFlowBreak(this.zombieListLayoutPanel.Controls[this.zombieListLayoutPanel.Controls.Count - 1], true);
            label = new Label();
            label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label.Text = "Unique zombies";
            label.TextAlign = ContentAlignment.BottomLeft;
            this.zombieListLayoutPanel.Controls.Add(label);
            this.zombieListLayoutPanel.SetFlowBreak(this.zombieListLayoutPanel.Controls[this.zombieListLayoutPanel.Controls.Count - 1], true);
            foreach (ZombieType zombie in this.UniqueZombies)
            {
                Button button = new Button();
                button.Name = "button-" + EnumUtil.ConvertToString(zombie);
                button.Size = new Size(40, 40);
                button.Margin = new System.Windows.Forms.Padding(0);
                button.Image = ImageHelper.GetZombieThumbnail(zombie);
                button.Click += new System.EventHandler(this.zombieListButton_onClick);
                this.zombieListLayoutPanel.Controls.Add(button);
            }

            this.zombieListLayoutPanel.SetFlowBreak(this.zombieListLayoutPanel.Controls[this.zombieListLayoutPanel.Controls.Count - 1], true);
            label = new Label();
            label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label.Text = "Special zombies";
            label.TextAlign = ContentAlignment.BottomLeft;
            this.zombieListLayoutPanel.Controls.Add(label);
            this.zombieListLayoutPanel.SetFlowBreak(this.zombieListLayoutPanel.Controls[this.zombieListLayoutPanel.Controls.Count - 1], true);
            foreach (ZombieType zombie in this.SpecialZombies)
            {
                Button button = new Button();
                button.Name = "button-" + EnumUtil.ConvertToString(zombie);
                button.Size = new Size(40, 40);
                button.Margin = new System.Windows.Forms.Padding(0);
                button.Image = ImageHelper.GetZombieThumbnail(zombie);
                button.Click += new System.EventHandler(this.zombieListButton_onClick);
                this.zombieListLayoutPanel.Controls.Add(button);
            }

            this.zombieListLayoutPanel.SetFlowBreak(this.zombieListLayoutPanel.Controls[this.zombieListLayoutPanel.Controls.Count - 1], true);
            label = new Label();
            label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label.Text = "Zombots";
            label.TextAlign = ContentAlignment.BottomLeft;
            this.zombieListLayoutPanel.Controls.Add(label);
            this.zombieListLayoutPanel.SetFlowBreak(this.zombieListLayoutPanel.Controls[this.zombieListLayoutPanel.Controls.Count - 1], true);
            foreach (ZombieType zombie in this.ZombotZombies)
            {
                Button button = new Button();
                button.Name = "button-" + EnumUtil.ConvertToString(zombie);
                button.Size = new Size(40, 40);
                button.Margin = new System.Windows.Forms.Padding(0);
                button.Image = ImageHelper.GetZombieThumbnail(zombie);
                button.Click += new System.EventHandler(this.zombieListButton_onClick);
                this.zombieListLayoutPanel.Controls.Add(button);
            }
        }

        private void zombieListButton_onClick(object sender, EventArgs e)
        {
            Button button = new Button();
            string row = this.zombieRowGroupBox.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked).Text;
            string zombie = ((Control)(sender)).Name.Split(new char[] { '-' }, 2)[1];
            if (row.Equals("None"))
            {
                row = "0";
            }
            button.Name = String.Format("selected{0}-{1}-{2}", Ordinal, zombie, row);
            button.Image = ImageHelper.GetZombieThumbnail(zombie);
            button.Size = new Size(40, 40);
            button.Margin = new System.Windows.Forms.Padding(0);
            button.Click += new System.EventHandler(this.zombieInWaveButton_onClick);
            this.ZombieInWaveLayoutPanel.Controls.Add(button);

            Label label = new Label();
            label.Name = String.Format("selected{0}", Ordinal++);
            label.Text = row;
            label.Size = new Size(12, 12);
            label.Margin = new System.Windows.Forms.Padding(0);
            this.ZombieInWaveLayoutPanel.Controls.Add(label);
        }

        private void zombieInWaveButton_onClick(object sender, EventArgs e)
        {
            string labelName = ((Control)(sender)).Name.Split(new char[] { '-' })[0];
            Label label = this.ZombieInWaveLayoutPanel.Controls.OfType<Label>().FirstOrDefault(n => n.Name.Equals(labelName));
            this.ZombieInWaveLayoutPanel.Controls.Remove((Button)(sender));
            this.ZombieInWaveLayoutPanel.Controls.Remove(label);
        }
    }
}
