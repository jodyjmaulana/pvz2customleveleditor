﻿using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvZ2CustomLevelEditor.Forms.Components
{
    public class PlantListLayoutPanel : FlowLayoutPanel
    {
        private FlowLayoutPanel DummyPanel { get; set; }
        private List<PlantType> AllPlants { get; set; }

        public PlantListLayoutPanel(FlowLayoutPanel panel)
        {
            this.DummyPanel = panel;
            this.InitializeComponents();
            this.InitializePlantList();
            this.PopulatePlantList();
        }

        private void InitializeComponents()
        {
            this.AutoScroll = true;
            this.Name = "plantListLayoutPanel";
            this.Size = this.DummyPanel.Size;
            this.Location = this.DummyPanel.Location;
            this.Anchor = this.DummyPanel.Anchor;
            this.MouseEnter += new EventHandler(panel_onMouseEnter);
        }

        private void InitializePlantList()
        {
            this.AllPlants = new List<PlantType>()
            {
                PlantType.sunflower,
                PlantType.peashooter,
                PlantType.wallnut,
                PlantType.potatomine,
                PlantType.bloomerang,
                PlantType.cabbagepult,
                PlantType.iceburg,
                PlantType.gravebuster,
                PlantType.twinsunflower,
                PlantType.bonkchoy,
                PlantType.repeater,
                PlantType.snowpea,
                PlantType.kernelpult,
                PlantType.snapdragon,
                PlantType.powerlily,
                PlantType.spikeweed,
                PlantType.coconutcannon,
                PlantType.cherry_bomb,
                PlantType.springbean,
                PlantType.spikerock,
                PlantType.threepeater,
                PlantType.squash,
                PlantType.splitpea,
                PlantType.chilibean,
                PlantType.torchwood,
                PlantType.lightningreed,
                PlantType.tallnut,
                PlantType.jalapeno,
                PlantType.peapod,
                PlantType.melonpult,
                PlantType.wintermelon,
                PlantType.imitater,
                PlantType.marigold,
                PlantType.laser_bean,
                PlantType.blover,
                PlantType.citron,
                PlantType.empea,
                PlantType.starfruit,
                PlantType.holonut,
                PlantType.magnifyinggrass,
                PlantType.powerplant,
                PlantType.hypnoshroom,
                PlantType.sunshroom,
                PlantType.puffshroom,
                PlantType.fumeshroom,
                PlantType.sunbean,
                PlantType.magnetshroom,
                PlantType.peanut,
                PlantType.chomper,
                PlantType.lilypad,
                PlantType.tanglekelp,
                PlantType.bowlingbulb,
                PlantType.ghostpepper,
                PlantType.homingthistle,
                PlantType.guacodile,
                PlantType.banana,
                PlantType.sweetpotato,
                PlantType.sapfling,
                PlantType.hurrikale,
                PlantType.hotpotato,
                PlantType.pepperpult,
                PlantType.chardguard,
                PlantType.firepeashooter,
                PlantType.stunion,
                PlantType.xshot,
                PlantType.dandelion,
                PlantType.lavaguava,
                PlantType.redstinger,
                PlantType.akee,
                PlantType.endurian,
                PlantType.toadstool,
                PlantType.stallia,
                PlantType.goldleaf,
                PlantType.strawburst,
                PlantType.cactus,
                PlantType.phatbeet,
                PlantType.celerystalker,
                PlantType.thymewarp,
                PlantType.electricblueberry,
                PlantType.garlic,
                PlantType.sporeshroom,
                PlantType.intensivecarrot,
                PlantType.jackolantern,
                PlantType.grapeshot,
                PlantType.primalpeashooter,
                PlantType.primalwallnut,
                PlantType.perfumeshroom,
                PlantType.coldsnapdragon,
                PlantType.primalsunflower,
                PlantType.primalpotatomine,
                PlantType.shrinkingviolet,
                PlantType.moonflower,
                PlantType.nightshade,
                PlantType.shadowshroom,
                PlantType.bloominghearts,
                PlantType.dusklobber,
                PlantType.escaperoot,
                PlantType.grimrose,
                PlantType.goldbloom,
                PlantType.electriccurrant,
                PlantType.aloe,
                PlantType.wasabiwhip,
                PlantType.explodeonut,
                PlantType.kiwibeast,
                PlantType.bombegranate,
                PlantType.applemortar,
                PlantType.witchhazel,
                PlantType.parsnip,
                PlantType.missiletoe,
                PlantType.hotdate,
                PlantType.caulipower,
                PlantType.solartomato,
                PlantType.electricpeashooter,
                PlantType.hollyknight
            };
        }

        private void PopulatePlantList()
        {
            foreach (PlantType plant in this.AllPlants)
            {
                Button button = new Button();
                button.Name = "button-" + EnumUtil.ConvertToString(plant);
                button.Size = new Size(40, 40);
                button.Margin = new System.Windows.Forms.Padding(0);
                button.Image = ImageHelper.GetPlantThumbnail(plant);
                if (plant == PlantType.imitater || plant == PlantType.marigold)
                {
                    button.Enabled = false;
                }
                button.MouseEnter += new EventHandler(panel_onMouseEnter);
                this.Controls.Add(button);
            }
        }

        private void panel_onMouseEnter(object sender, EventArgs e)
        {
            this.Focus();
        }
    }
}
