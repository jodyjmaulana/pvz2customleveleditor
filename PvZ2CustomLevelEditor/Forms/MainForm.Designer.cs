﻿namespace PvZ2CustomLevelEditor.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.levelTabPage = new System.Windows.Forms.TabPage();
            this.gameFeaturesToUnlockLabel = new System.Windows.Forms.Panel();
            this.tutorialIntroComboBox = new System.Windows.Forms.ComboBox();
            this.tutorialIntroLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gameFeaturesToUnlockListBox = new System.Windows.Forms.ListBox();
            this.supressDynamicTutorialCheckBox = new System.Windows.Forms.CheckBox();
            this.repeatPlayForceToWorldMapCheckBox = new System.Windows.Forms.CheckBox();
            this.tutorialPeashooterDeathCheckBox = new System.Windows.Forms.CheckBox();
            this.firstIntroNarrativeComboBox = new System.Windows.Forms.ComboBox();
            this.firstOutroNarrativeComboBox = new System.Windows.Forms.ComboBox();
            this.seedBankPropButton = new System.Windows.Forms.Button();
            this.seedBankPropLabel = new System.Windows.Forms.Label();
            this.miniGamePropLabel = new System.Windows.Forms.Label();
            this.miniGamePropButton = new System.Windows.Forms.Button();
            this.musicTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.musicBRadioButton = new System.Windows.Forms.RadioButton();
            this.musicARadioButton = new System.Windows.Forms.RadioButton();
            this.musicDefaultRadioButton = new System.Windows.Forms.RadioButton();
            this.forceToWorldMapCheckBox = new System.Windows.Forms.CheckBox();
            this.firstRewardTypeComboBox = new System.Windows.Forms.ComboBox();
            this.firstRewardParamComboBox = new System.Windows.Forms.ComboBox();
            this.firstRewardTypeLabel = new System.Windows.Forms.Label();
            this.firstRewardParamLabel = new System.Windows.Forms.Label();
            this.firstOutroNarrativeLabel = new System.Windows.Forms.Label();
            this.firstIntroNarrativeLabel = new System.Windows.Forms.Label();
            this.sunDropCheckBox = new System.Windows.Forms.CheckBox();
            this.mowerCheckBox = new System.Windows.Forms.CheckBox();
            this.commentLabel = new System.Windows.Forms.Label();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.levelNumBox = new System.Windows.Forms.NumericUpDown();
            this.levelNumLabel = new System.Windows.Forms.Label();
            this.levelWorldTypeComboBox = new System.Windows.Forms.ComboBox();
            this.levelWorldTypeLabel = new System.Windows.Forms.Label();
            this.levelDescDefaultCheckBox = new System.Windows.Forms.CheckBox();
            this.levelDescTextBox = new System.Windows.Forms.TextBox();
            this.levelDescLabel = new System.Windows.Forms.Label();
            this.levelNameDefaultCheckBox = new System.Windows.Forms.CheckBox();
            this.levelNameTextBox = new System.Windows.Forms.TextBox();
            this.levelNameLabel = new System.Windows.Forms.Label();
            this.lawnTabPage = new System.Windows.Forms.TabPage();
            this.waveTabPage = new System.Windows.Forms.TabPage();
            this.waveTabPanel = new System.Windows.Forms.Panel();
            this.wavePanel = new System.Windows.Forms.Panel();
            this.generateWaveButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dynamicWavePropLabel = new System.Windows.Forms.Label();
            this.waveSpendingPointsIncrementNumBox = new System.Windows.Forms.NumericUpDown();
            this.waveSpendingPointsIncrementLabel = new System.Windows.Forms.Label();
            this.waveSpendingPointsNumBox = new System.Windows.Forms.NumericUpDown();
            this.waveSpendingPointsLabel = new System.Windows.Forms.Label();
            this.maxNextWaveHealthNumBox = new System.Windows.Forms.NumericUpDown();
            this.maxNextWaveHealthLabel = new System.Windows.Forms.Label();
            this.minNextWaveHealthNumBox = new System.Windows.Forms.NumericUpDown();
            this.minNextWaveHealthLabel = new System.Windows.Forms.Label();
            this.waveNumBox = new System.Windows.Forms.NumericUpDown();
            this.numWaveLabel = new System.Windows.Forms.Label();
            this.flagNumBox = new System.Windows.Forms.NumericUpDown();
            this.numFlagLabel = new System.Windows.Forms.Label();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateWaveWorker = new System.ComponentModel.BackgroundWorker();
            this.generateWaveProgressBar = new System.Windows.Forms.ProgressBar();
            this.generateWaveProgressLabel = new System.Windows.Forms.Label();
            this.mainTabControl.SuspendLayout();
            this.levelTabPage.SuspendLayout();
            this.gameFeaturesToUnlockLabel.SuspendLayout();
            this.musicTypeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.levelNumBox)).BeginInit();
            this.waveTabPage.SuspendLayout();
            this.waveTabPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waveSpendingPointsIncrementNumBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveSpendingPointsNumBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNextWaveHealthNumBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minNextWaveHealthNumBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveNumBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flagNumBox)).BeginInit();
            this.mainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTabControl.Controls.Add(this.levelTabPage);
            this.mainTabControl.Controls.Add(this.lawnTabPage);
            this.mainTabControl.Controls.Add(this.waveTabPage);
            this.mainTabControl.Location = new System.Drawing.Point(12, 27);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(691, 420);
            this.mainTabControl.TabIndex = 0;
            // 
            // levelTabPage
            // 
            this.levelTabPage.AutoScroll = true;
            this.levelTabPage.Controls.Add(this.gameFeaturesToUnlockLabel);
            this.levelTabPage.Location = new System.Drawing.Point(4, 22);
            this.levelTabPage.Name = "levelTabPage";
            this.levelTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.levelTabPage.Size = new System.Drawing.Size(683, 394);
            this.levelTabPage.TabIndex = 0;
            this.levelTabPage.Text = "Level Properties";
            this.levelTabPage.UseVisualStyleBackColor = true;
            // 
            // gameFeaturesToUnlockLabel
            // 
            this.gameFeaturesToUnlockLabel.BackColor = System.Drawing.Color.Transparent;
            this.gameFeaturesToUnlockLabel.Controls.Add(this.tutorialIntroComboBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.tutorialIntroLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.label1);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.gameFeaturesToUnlockListBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.supressDynamicTutorialCheckBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.repeatPlayForceToWorldMapCheckBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.tutorialPeashooterDeathCheckBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.firstIntroNarrativeComboBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.firstOutroNarrativeComboBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.seedBankPropButton);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.seedBankPropLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.miniGamePropLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.miniGamePropButton);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.musicTypeGroupBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.forceToWorldMapCheckBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.firstRewardTypeComboBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.firstRewardParamComboBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.firstRewardTypeLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.firstRewardParamLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.firstOutroNarrativeLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.firstIntroNarrativeLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.sunDropCheckBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.mowerCheckBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.commentLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.commentTextBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelNumBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelNumLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelWorldTypeComboBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelWorldTypeLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelDescDefaultCheckBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelDescTextBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelDescLabel);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelNameDefaultCheckBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelNameTextBox);
            this.gameFeaturesToUnlockLabel.Controls.Add(this.levelNameLabel);
            this.gameFeaturesToUnlockLabel.Location = new System.Drawing.Point(0, 0);
            this.gameFeaturesToUnlockLabel.Name = "gameFeaturesToUnlockLabel";
            this.gameFeaturesToUnlockLabel.Size = new System.Drawing.Size(663, 394);
            this.gameFeaturesToUnlockLabel.TabIndex = 0;
            this.gameFeaturesToUnlockLabel.MouseEnter += new System.EventHandler(this.panel_onMouseEnter);
            // 
            // tutorialIntroComboBox
            // 
            this.tutorialIntroComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tutorialIntroComboBox.FormattingEnabled = true;
            this.tutorialIntroComboBox.Location = new System.Drawing.Point(27, 363);
            this.tutorialIntroComboBox.Name = "tutorialIntroComboBox";
            this.tutorialIntroComboBox.Size = new System.Drawing.Size(214, 21);
            this.tutorialIntroComboBox.TabIndex = 39;
            // 
            // tutorialIntroLabel
            // 
            this.tutorialIntroLabel.AutoSize = true;
            this.tutorialIntroLabel.Location = new System.Drawing.Point(24, 346);
            this.tutorialIntroLabel.Name = "tutorialIntroLabel";
            this.tutorialIntroLabel.Size = new System.Drawing.Size(66, 13);
            this.tutorialIntroLabel.TabIndex = 38;
            this.tutorialIntroLabel.Text = "Tutorial Intro";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(293, 252);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Game Features To Unlock";
            // 
            // gameFeaturesToUnlockListBox
            // 
            this.gameFeaturesToUnlockListBox.FormattingEnabled = true;
            this.gameFeaturesToUnlockListBox.Location = new System.Drawing.Point(296, 270);
            this.gameFeaturesToUnlockListBox.Name = "gameFeaturesToUnlockListBox";
            this.gameFeaturesToUnlockListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.gameFeaturesToUnlockListBox.Size = new System.Drawing.Size(145, 69);
            this.gameFeaturesToUnlockListBox.TabIndex = 36;
            // 
            // supressDynamicTutorialCheckBox
            // 
            this.supressDynamicTutorialCheckBox.AutoSize = true;
            this.supressDynamicTutorialCheckBox.Location = new System.Drawing.Point(502, 253);
            this.supressDynamicTutorialCheckBox.Name = "supressDynamicTutorialCheckBox";
            this.supressDynamicTutorialCheckBox.Size = new System.Drawing.Size(151, 17);
            this.supressDynamicTutorialCheckBox.TabIndex = 35;
            this.supressDynamicTutorialCheckBox.Text = "Supress Dynamic Difficulty";
            this.supressDynamicTutorialCheckBox.UseVisualStyleBackColor = true;
            // 
            // repeatPlayForceToWorldMapCheckBox
            // 
            this.repeatPlayForceToWorldMapCheckBox.AutoSize = true;
            this.repeatPlayForceToWorldMapCheckBox.Location = new System.Drawing.Point(502, 229);
            this.repeatPlayForceToWorldMapCheckBox.Name = "repeatPlayForceToWorldMapCheckBox";
            this.repeatPlayForceToWorldMapCheckBox.Size = new System.Drawing.Size(156, 17);
            this.repeatPlayForceToWorldMapCheckBox.TabIndex = 34;
            this.repeatPlayForceToWorldMapCheckBox.Text = "Repat Force To World Map";
            this.repeatPlayForceToWorldMapCheckBox.UseVisualStyleBackColor = true;
            // 
            // tutorialPeashooterDeathCheckBox
            // 
            this.tutorialPeashooterDeathCheckBox.AutoSize = true;
            this.tutorialPeashooterDeathCheckBox.Location = new System.Drawing.Point(502, 205);
            this.tutorialPeashooterDeathCheckBox.Name = "tutorialPeashooterDeathCheckBox";
            this.tutorialPeashooterDeathCheckBox.Size = new System.Drawing.Size(150, 17);
            this.tutorialPeashooterDeathCheckBox.TabIndex = 33;
            this.tutorialPeashooterDeathCheckBox.Text = "Tutorial Peashooter Death";
            this.tutorialPeashooterDeathCheckBox.UseVisualStyleBackColor = true;
            // 
            // firstIntroNarrativeComboBox
            // 
            this.firstIntroNarrativeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.firstIntroNarrativeComboBox.FormattingEnabled = true;
            this.firstIntroNarrativeComboBox.Location = new System.Drawing.Point(27, 184);
            this.firstIntroNarrativeComboBox.Name = "firstIntroNarrativeComboBox";
            this.firstIntroNarrativeComboBox.Size = new System.Drawing.Size(214, 21);
            this.firstIntroNarrativeComboBox.TabIndex = 32;
            // 
            // firstOutroNarrativeComboBox
            // 
            this.firstOutroNarrativeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.firstOutroNarrativeComboBox.FormattingEnabled = true;
            this.firstOutroNarrativeComboBox.Location = new System.Drawing.Point(27, 224);
            this.firstOutroNarrativeComboBox.Name = "firstOutroNarrativeComboBox";
            this.firstOutroNarrativeComboBox.Size = new System.Drawing.Size(214, 21);
            this.firstOutroNarrativeComboBox.TabIndex = 31;
            // 
            // seedBankPropButton
            // 
            this.seedBankPropButton.Location = new System.Drawing.Point(508, 176);
            this.seedBankPropButton.Name = "seedBankPropButton";
            this.seedBankPropButton.Size = new System.Drawing.Size(89, 23);
            this.seedBankPropButton.TabIndex = 30;
            this.seedBankPropButton.Text = "Properties";
            this.seedBankPropButton.UseVisualStyleBackColor = true;
            this.seedBankPropButton.Click += new System.EventHandler(this.seedBankPropButton_onClick);
            // 
            // seedBankPropLabel
            // 
            this.seedBankPropLabel.AutoSize = true;
            this.seedBankPropLabel.Location = new System.Drawing.Point(505, 159);
            this.seedBankPropLabel.Name = "seedBankPropLabel";
            this.seedBankPropLabel.Size = new System.Drawing.Size(60, 13);
            this.seedBankPropLabel.TabIndex = 29;
            this.seedBankPropLabel.Text = "Seed Bank";
            // 
            // miniGamePropLabel
            // 
            this.miniGamePropLabel.AutoSize = true;
            this.miniGamePropLabel.Location = new System.Drawing.Point(502, 113);
            this.miniGamePropLabel.Name = "miniGamePropLabel";
            this.miniGamePropLabel.Size = new System.Drawing.Size(62, 13);
            this.miniGamePropLabel.TabIndex = 28;
            this.miniGamePropLabel.Text = "Mini Games";
            // 
            // miniGamePropButton
            // 
            this.miniGamePropButton.Location = new System.Drawing.Point(505, 129);
            this.miniGamePropButton.Name = "miniGamePropButton";
            this.miniGamePropButton.Size = new System.Drawing.Size(92, 23);
            this.miniGamePropButton.TabIndex = 27;
            this.miniGamePropButton.Text = "Properties";
            this.miniGamePropButton.UseVisualStyleBackColor = true;
            this.miniGamePropButton.Click += new System.EventHandler(this.miniGamePropButton_onClick);
            // 
            // musicTypeGroupBox
            // 
            this.musicTypeGroupBox.Controls.Add(this.musicBRadioButton);
            this.musicTypeGroupBox.Controls.Add(this.musicARadioButton);
            this.musicTypeGroupBox.Controls.Add(this.musicDefaultRadioButton);
            this.musicTypeGroupBox.Location = new System.Drawing.Point(27, 251);
            this.musicTypeGroupBox.Name = "musicTypeGroupBox";
            this.musicTypeGroupBox.Size = new System.Drawing.Size(93, 92);
            this.musicTypeGroupBox.TabIndex = 26;
            this.musicTypeGroupBox.TabStop = false;
            this.musicTypeGroupBox.Text = "Music Type";
            // 
            // musicBRadioButton
            // 
            this.musicBRadioButton.AutoSize = true;
            this.musicBRadioButton.Location = new System.Drawing.Point(6, 65);
            this.musicBRadioButton.Name = "musicBRadioButton";
            this.musicBRadioButton.Size = new System.Drawing.Size(62, 17);
            this.musicBRadioButton.TabIndex = 27;
            this.musicBRadioButton.Text = "Music 2";
            this.musicBRadioButton.UseVisualStyleBackColor = true;
            // 
            // musicARadioButton
            // 
            this.musicARadioButton.AutoSize = true;
            this.musicARadioButton.Location = new System.Drawing.Point(6, 42);
            this.musicARadioButton.Name = "musicARadioButton";
            this.musicARadioButton.Size = new System.Drawing.Size(62, 17);
            this.musicARadioButton.TabIndex = 26;
            this.musicARadioButton.Text = "Music 1";
            this.musicARadioButton.UseVisualStyleBackColor = true;
            // 
            // musicDefaultRadioButton
            // 
            this.musicDefaultRadioButton.AutoSize = true;
            this.musicDefaultRadioButton.Checked = true;
            this.musicDefaultRadioButton.Location = new System.Drawing.Point(6, 19);
            this.musicDefaultRadioButton.Name = "musicDefaultRadioButton";
            this.musicDefaultRadioButton.Size = new System.Drawing.Size(59, 17);
            this.musicDefaultRadioButton.TabIndex = 25;
            this.musicDefaultRadioButton.TabStop = true;
            this.musicDefaultRadioButton.Text = "Default";
            this.musicDefaultRadioButton.UseVisualStyleBackColor = true;
            // 
            // forceToWorldMapCheckBox
            // 
            this.forceToWorldMapCheckBox.AutoSize = true;
            this.forceToWorldMapCheckBox.Location = new System.Drawing.Point(502, 89);
            this.forceToWorldMapCheckBox.Name = "forceToWorldMapCheckBox";
            this.forceToWorldMapCheckBox.Size = new System.Drawing.Size(124, 17);
            this.forceToWorldMapCheckBox.TabIndex = 24;
            this.forceToWorldMapCheckBox.Text = "Force To World Map";
            this.forceToWorldMapCheckBox.UseVisualStyleBackColor = true;
            // 
            // firstRewardTypeComboBox
            // 
            this.firstRewardTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.firstRewardTypeComboBox.FormattingEnabled = true;
            this.firstRewardTypeComboBox.Location = new System.Drawing.Point(293, 184);
            this.firstRewardTypeComboBox.Name = "firstRewardTypeComboBox";
            this.firstRewardTypeComboBox.Size = new System.Drawing.Size(148, 21);
            this.firstRewardTypeComboBox.TabIndex = 23;
            this.firstRewardTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.firstRewardTypeComboBox_onSelectedIndexChanged);
            // 
            // firstRewardParamComboBox
            // 
            this.firstRewardParamComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.firstRewardParamComboBox.FormattingEnabled = true;
            this.firstRewardParamComboBox.Location = new System.Drawing.Point(293, 224);
            this.firstRewardParamComboBox.Name = "firstRewardParamComboBox";
            this.firstRewardParamComboBox.Size = new System.Drawing.Size(148, 21);
            this.firstRewardParamComboBox.TabIndex = 22;
            // 
            // firstRewardTypeLabel
            // 
            this.firstRewardTypeLabel.AutoSize = true;
            this.firstRewardTypeLabel.Location = new System.Drawing.Point(290, 167);
            this.firstRewardTypeLabel.Name = "firstRewardTypeLabel";
            this.firstRewardTypeLabel.Size = new System.Drawing.Size(93, 13);
            this.firstRewardTypeLabel.TabIndex = 17;
            this.firstRewardTypeLabel.Text = "First Reward Type";
            // 
            // firstRewardParamLabel
            // 
            this.firstRewardParamLabel.AutoSize = true;
            this.firstRewardParamLabel.Location = new System.Drawing.Point(290, 207);
            this.firstRewardParamLabel.Name = "firstRewardParamLabel";
            this.firstRewardParamLabel.Size = new System.Drawing.Size(117, 13);
            this.firstRewardParamLabel.TabIndex = 16;
            this.firstRewardParamLabel.Text = "First Reward Parameter";
            // 
            // firstOutroNarrativeLabel
            // 
            this.firstOutroNarrativeLabel.AutoSize = true;
            this.firstOutroNarrativeLabel.Location = new System.Drawing.Point(24, 207);
            this.firstOutroNarrativeLabel.Name = "firstOutroNarrativeLabel";
            this.firstOutroNarrativeLabel.Size = new System.Drawing.Size(101, 13);
            this.firstOutroNarrativeLabel.TabIndex = 15;
            this.firstOutroNarrativeLabel.Text = "First Outro Narrative";
            // 
            // firstIntroNarrativeLabel
            // 
            this.firstIntroNarrativeLabel.AutoSize = true;
            this.firstIntroNarrativeLabel.Location = new System.Drawing.Point(24, 167);
            this.firstIntroNarrativeLabel.Name = "firstIntroNarrativeLabel";
            this.firstIntroNarrativeLabel.Size = new System.Drawing.Size(96, 13);
            this.firstIntroNarrativeLabel.TabIndex = 14;
            this.firstIntroNarrativeLabel.Text = "First Intro Narrative";
            // 
            // sunDropCheckBox
            // 
            this.sunDropCheckBox.AutoSize = true;
            this.sunDropCheckBox.Checked = true;
            this.sunDropCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.sunDropCheckBox.Location = new System.Drawing.Point(502, 65);
            this.sunDropCheckBox.Name = "sunDropCheckBox";
            this.sunDropCheckBox.Size = new System.Drawing.Size(71, 17);
            this.sunDropCheckBox.TabIndex = 13;
            this.sunDropCheckBox.Text = "Sun Drop";
            this.sunDropCheckBox.UseVisualStyleBackColor = true;
            // 
            // mowerCheckBox
            // 
            this.mowerCheckBox.AutoSize = true;
            this.mowerCheckBox.Checked = true;
            this.mowerCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mowerCheckBox.Location = new System.Drawing.Point(502, 41);
            this.mowerCheckBox.Name = "mowerCheckBox";
            this.mowerCheckBox.Size = new System.Drawing.Size(92, 17);
            this.mowerCheckBox.TabIndex = 12;
            this.mowerCheckBox.Text = "Lawn Mowers";
            this.mowerCheckBox.UseVisualStyleBackColor = true;
            // 
            // commentLabel
            // 
            this.commentLabel.AutoSize = true;
            this.commentLabel.Location = new System.Drawing.Point(24, 25);
            this.commentLabel.Name = "commentLabel";
            this.commentLabel.Size = new System.Drawing.Size(51, 13);
            this.commentLabel.TabIndex = 11;
            this.commentLabel.Text = "Comment";
            // 
            // commentTextBox
            // 
            this.commentTextBox.Location = new System.Drawing.Point(27, 41);
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.Size = new System.Drawing.Size(414, 20);
            this.commentTextBox.TabIndex = 10;
            // 
            // levelNumBox
            // 
            this.levelNumBox.Location = new System.Drawing.Point(293, 120);
            this.levelNumBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.levelNumBox.Name = "levelNumBox";
            this.levelNumBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.levelNumBox.Size = new System.Drawing.Size(148, 20);
            this.levelNumBox.TabIndex = 9;
            this.levelNumBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // levelNumLabel
            // 
            this.levelNumLabel.AutoSize = true;
            this.levelNumLabel.Location = new System.Drawing.Point(290, 104);
            this.levelNumLabel.Name = "levelNumLabel";
            this.levelNumLabel.Size = new System.Drawing.Size(73, 13);
            this.levelNumLabel.TabIndex = 8;
            this.levelNumLabel.Text = "Level Number";
            // 
            // levelWorldTypeComboBox
            // 
            this.levelWorldTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.levelWorldTypeComboBox.FormattingEnabled = true;
            this.levelWorldTypeComboBox.Location = new System.Drawing.Point(293, 81);
            this.levelWorldTypeComboBox.Name = "levelWorldTypeComboBox";
            this.levelWorldTypeComboBox.Size = new System.Drawing.Size(148, 21);
            this.levelWorldTypeComboBox.TabIndex = 7;
            this.levelWorldTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.levelWorldTypeComboBox_onSelectedIndexChanged);
            // 
            // levelWorldTypeLabel
            // 
            this.levelWorldTypeLabel.AutoSize = true;
            this.levelWorldTypeLabel.Location = new System.Drawing.Point(290, 64);
            this.levelWorldTypeLabel.Name = "levelWorldTypeLabel";
            this.levelWorldTypeLabel.Size = new System.Drawing.Size(62, 13);
            this.levelWorldTypeLabel.TabIndex = 6;
            this.levelWorldTypeLabel.Text = "World Type";
            // 
            // levelDescDefaultCheckBox
            // 
            this.levelDescDefaultCheckBox.AutoSize = true;
            this.levelDescDefaultCheckBox.Checked = true;
            this.levelDescDefaultCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.levelDescDefaultCheckBox.Location = new System.Drawing.Point(181, 123);
            this.levelDescDefaultCheckBox.Name = "levelDescDefaultCheckBox";
            this.levelDescDefaultCheckBox.Size = new System.Drawing.Size(60, 17);
            this.levelDescDefaultCheckBox.TabIndex = 5;
            this.levelDescDefaultCheckBox.Text = "Default";
            this.levelDescDefaultCheckBox.UseVisualStyleBackColor = true;
            this.levelDescDefaultCheckBox.CheckedChanged += new System.EventHandler(this.levelDescDefaultCheckBox_onCheckedChanged);
            // 
            // levelDescTextBox
            // 
            this.levelDescTextBox.Enabled = false;
            this.levelDescTextBox.Location = new System.Drawing.Point(27, 121);
            this.levelDescTextBox.Name = "levelDescTextBox";
            this.levelDescTextBox.Size = new System.Drawing.Size(148, 20);
            this.levelDescTextBox.TabIndex = 4;
            // 
            // levelDescLabel
            // 
            this.levelDescLabel.AutoSize = true;
            this.levelDescLabel.Location = new System.Drawing.Point(24, 104);
            this.levelDescLabel.Name = "levelDescLabel";
            this.levelDescLabel.Size = new System.Drawing.Size(89, 13);
            this.levelDescLabel.TabIndex = 3;
            this.levelDescLabel.Text = "Level Description";
            // 
            // levelNameDefaultCheckBox
            // 
            this.levelNameDefaultCheckBox.AutoSize = true;
            this.levelNameDefaultCheckBox.Checked = true;
            this.levelNameDefaultCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.levelNameDefaultCheckBox.Location = new System.Drawing.Point(181, 83);
            this.levelNameDefaultCheckBox.Name = "levelNameDefaultCheckBox";
            this.levelNameDefaultCheckBox.Size = new System.Drawing.Size(60, 17);
            this.levelNameDefaultCheckBox.TabIndex = 2;
            this.levelNameDefaultCheckBox.Text = "Default";
            this.levelNameDefaultCheckBox.UseVisualStyleBackColor = true;
            this.levelNameDefaultCheckBox.CheckedChanged += new System.EventHandler(this.levelNameDefaultCheckBox_onCheckedChanged);
            // 
            // levelNameTextBox
            // 
            this.levelNameTextBox.Enabled = false;
            this.levelNameTextBox.Location = new System.Drawing.Point(27, 81);
            this.levelNameTextBox.Name = "levelNameTextBox";
            this.levelNameTextBox.Size = new System.Drawing.Size(148, 20);
            this.levelNameTextBox.TabIndex = 1;
            // 
            // levelNameLabel
            // 
            this.levelNameLabel.AutoSize = true;
            this.levelNameLabel.Location = new System.Drawing.Point(24, 64);
            this.levelNameLabel.Name = "levelNameLabel";
            this.levelNameLabel.Size = new System.Drawing.Size(64, 13);
            this.levelNameLabel.TabIndex = 0;
            this.levelNameLabel.Text = "Level Name";
            // 
            // lawnTabPage
            // 
            this.lawnTabPage.Location = new System.Drawing.Point(4, 22);
            this.lawnTabPage.Name = "lawnTabPage";
            this.lawnTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.lawnTabPage.Size = new System.Drawing.Size(683, 394);
            this.lawnTabPage.TabIndex = 1;
            this.lawnTabPage.Text = "Lawn Properties";
            this.lawnTabPage.UseVisualStyleBackColor = true;
            // 
            // waveTabPage
            // 
            this.waveTabPage.AutoScroll = true;
            this.waveTabPage.Controls.Add(this.waveTabPanel);
            this.waveTabPage.Location = new System.Drawing.Point(4, 22);
            this.waveTabPage.Name = "waveTabPage";
            this.waveTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.waveTabPage.Size = new System.Drawing.Size(683, 394);
            this.waveTabPage.TabIndex = 2;
            this.waveTabPage.Text = "Wave Properties";
            this.waveTabPage.UseVisualStyleBackColor = true;
            // 
            // waveTabPanel
            // 
            this.waveTabPanel.BackColor = System.Drawing.Color.Transparent;
            this.waveTabPanel.Controls.Add(this.wavePanel);
            this.waveTabPanel.Controls.Add(this.generateWaveButton);
            this.waveTabPanel.Controls.Add(this.button1);
            this.waveTabPanel.Controls.Add(this.dynamicWavePropLabel);
            this.waveTabPanel.Controls.Add(this.waveSpendingPointsIncrementNumBox);
            this.waveTabPanel.Controls.Add(this.waveSpendingPointsIncrementLabel);
            this.waveTabPanel.Controls.Add(this.waveSpendingPointsNumBox);
            this.waveTabPanel.Controls.Add(this.waveSpendingPointsLabel);
            this.waveTabPanel.Controls.Add(this.maxNextWaveHealthNumBox);
            this.waveTabPanel.Controls.Add(this.maxNextWaveHealthLabel);
            this.waveTabPanel.Controls.Add(this.minNextWaveHealthNumBox);
            this.waveTabPanel.Controls.Add(this.minNextWaveHealthLabel);
            this.waveTabPanel.Controls.Add(this.waveNumBox);
            this.waveTabPanel.Controls.Add(this.numWaveLabel);
            this.waveTabPanel.Controls.Add(this.flagNumBox);
            this.waveTabPanel.Controls.Add(this.numFlagLabel);
            this.waveTabPanel.Location = new System.Drawing.Point(0, 0);
            this.waveTabPanel.Name = "waveTabPanel";
            this.waveTabPanel.Size = new System.Drawing.Size(663, 503);
            this.waveTabPanel.TabIndex = 0;
            this.waveTabPanel.MouseEnter += new System.EventHandler(this.panel_onMouseEnter);
            // 
            // wavePanel
            // 
            this.wavePanel.AutoScroll = true;
            this.wavePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.wavePanel.Location = new System.Drawing.Point(27, 197);
            this.wavePanel.Name = "wavePanel";
            this.wavePanel.Size = new System.Drawing.Size(610, 285);
            this.wavePanel.TabIndex = 31;
            // 
            // generateWaveButton
            // 
            this.generateWaveButton.Location = new System.Drawing.Point(27, 168);
            this.generateWaveButton.Name = "generateWaveButton";
            this.generateWaveButton.Size = new System.Drawing.Size(123, 23);
            this.generateWaveButton.TabIndex = 30;
            this.generateWaveButton.Text = "Generate Wave";
            this.generateWaveButton.UseVisualStyleBackColor = true;
            this.generateWaveButton.Click += new System.EventHandler(this.generateWaveButton_onClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(505, 41);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "Properties";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dynamicWavePropLabel
            // 
            this.dynamicWavePropLabel.AutoSize = true;
            this.dynamicWavePropLabel.Location = new System.Drawing.Point(502, 25);
            this.dynamicWavePropLabel.Name = "dynamicWavePropLabel";
            this.dynamicWavePropLabel.Size = new System.Drawing.Size(80, 13);
            this.dynamicWavePropLabel.TabIndex = 12;
            this.dynamicWavePropLabel.Text = "Dynamic Wave";
            // 
            // waveSpendingPointsIncrementNumBox
            // 
            this.waveSpendingPointsIncrementNumBox.Location = new System.Drawing.Point(293, 119);
            this.waveSpendingPointsIncrementNumBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.waveSpendingPointsIncrementNumBox.Name = "waveSpendingPointsIncrementNumBox";
            this.waveSpendingPointsIncrementNumBox.Size = new System.Drawing.Size(148, 20);
            this.waveSpendingPointsIncrementNumBox.TabIndex = 11;
            this.waveSpendingPointsIncrementNumBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // waveSpendingPointsIncrementLabel
            // 
            this.waveSpendingPointsIncrementLabel.AutoSize = true;
            this.waveSpendingPointsIncrementLabel.Location = new System.Drawing.Point(290, 103);
            this.waveSpendingPointsIncrementLabel.Name = "waveSpendingPointsIncrementLabel";
            this.waveSpendingPointsIncrementLabel.Size = new System.Drawing.Size(166, 13);
            this.waveSpendingPointsIncrementLabel.TabIndex = 10;
            this.waveSpendingPointsIncrementLabel.Text = "Wave Spending Points Increment";
            // 
            // waveSpendingPointsNumBox
            // 
            this.waveSpendingPointsNumBox.Location = new System.Drawing.Point(27, 119);
            this.waveSpendingPointsNumBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.waveSpendingPointsNumBox.Name = "waveSpendingPointsNumBox";
            this.waveSpendingPointsNumBox.Size = new System.Drawing.Size(148, 20);
            this.waveSpendingPointsNumBox.TabIndex = 9;
            this.waveSpendingPointsNumBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // waveSpendingPointsLabel
            // 
            this.waveSpendingPointsLabel.AutoSize = true;
            this.waveSpendingPointsLabel.Location = new System.Drawing.Point(24, 103);
            this.waveSpendingPointsLabel.Name = "waveSpendingPointsLabel";
            this.waveSpendingPointsLabel.Size = new System.Drawing.Size(116, 13);
            this.waveSpendingPointsLabel.TabIndex = 8;
            this.waveSpendingPointsLabel.Text = "Wave Spending Points";
            // 
            // maxNextWaveHealthNumBox
            // 
            this.maxNextWaveHealthNumBox.Location = new System.Drawing.Point(293, 80);
            this.maxNextWaveHealthNumBox.Name = "maxNextWaveHealthNumBox";
            this.maxNextWaveHealthNumBox.Size = new System.Drawing.Size(148, 20);
            this.maxNextWaveHealthNumBox.TabIndex = 7;
            // 
            // maxNextWaveHealthLabel
            // 
            this.maxNextWaveHealthLabel.AutoSize = true;
            this.maxNextWaveHealthLabel.Location = new System.Drawing.Point(290, 64);
            this.maxNextWaveHealthLabel.Name = "maxNextWaveHealthLabel";
            this.maxNextWaveHealthLabel.Size = new System.Drawing.Size(129, 13);
            this.maxNextWaveHealthLabel.TabIndex = 6;
            this.maxNextWaveHealthLabel.Text = "Max Next Wave Health %";
            // 
            // minNextWaveHealthNumBox
            // 
            this.minNextWaveHealthNumBox.Location = new System.Drawing.Point(27, 80);
            this.minNextWaveHealthNumBox.Name = "minNextWaveHealthNumBox";
            this.minNextWaveHealthNumBox.Size = new System.Drawing.Size(148, 20);
            this.minNextWaveHealthNumBox.TabIndex = 5;
            // 
            // minNextWaveHealthLabel
            // 
            this.minNextWaveHealthLabel.AutoSize = true;
            this.minNextWaveHealthLabel.Location = new System.Drawing.Point(24, 64);
            this.minNextWaveHealthLabel.Name = "minNextWaveHealthLabel";
            this.minNextWaveHealthLabel.Size = new System.Drawing.Size(126, 13);
            this.minNextWaveHealthLabel.TabIndex = 4;
            this.minNextWaveHealthLabel.Text = "Min Next Wave Health %";
            // 
            // waveNumBox
            // 
            this.waveNumBox.Location = new System.Drawing.Point(293, 41);
            this.waveNumBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.waveNumBox.Name = "waveNumBox";
            this.waveNumBox.Size = new System.Drawing.Size(148, 20);
            this.waveNumBox.TabIndex = 3;
            this.waveNumBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numWaveLabel
            // 
            this.numWaveLabel.AutoSize = true;
            this.numWaveLabel.Location = new System.Drawing.Point(290, 25);
            this.numWaveLabel.Name = "numWaveLabel";
            this.numWaveLabel.Size = new System.Drawing.Size(82, 13);
            this.numWaveLabel.TabIndex = 2;
            this.numWaveLabel.Text = "Waves per Flag";
            // 
            // flagNumBox
            // 
            this.flagNumBox.Location = new System.Drawing.Point(27, 41);
            this.flagNumBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.flagNumBox.Name = "flagNumBox";
            this.flagNumBox.Size = new System.Drawing.Size(148, 20);
            this.flagNumBox.TabIndex = 1;
            this.flagNumBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numFlagLabel
            // 
            this.numFlagLabel.AutoSize = true;
            this.numFlagLabel.Location = new System.Drawing.Point(24, 25);
            this.numFlagLabel.Name = "numFlagLabel";
            this.numFlagLabel.Size = new System.Drawing.Size(84, 13);
            this.numFlagLabel.TabIndex = 0;
            this.numFlagLabel.Text = "Number of Flags";
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(715, 24);
            this.mainMenuStrip.TabIndex = 1;
            this.mainMenuStrip.Text = "Main Menu Strip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.advancedModeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.newToolStripMenuItem.Text = "New";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_onClick);
            // 
            // advancedModeToolStripMenuItem
            // 
            this.advancedModeToolStripMenuItem.Name = "advancedModeToolStripMenuItem";
            this.advancedModeToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.advancedModeToolStripMenuItem.Text = "Advanced Mode";
            this.advancedModeToolStripMenuItem.Click += new System.EventHandler(this.advancedModeToolStripMenuItem_onClick);
            // 
            // generateWaveWorker
            // 
            this.generateWaveWorker.WorkerReportsProgress = true;
            this.generateWaveWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.generateWaveWorker_doWork);
            this.generateWaveWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.generateWaveWorker_onProgressChanged);
            this.generateWaveWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.generateWaveWorker_onRunWorkerCompleted);
            // 
            // generateWaveProgressBar
            // 
            this.generateWaveProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.generateWaveProgressBar.Location = new System.Drawing.Point(12, 453);
            this.generateWaveProgressBar.Name = "generateWaveProgressBar";
            this.generateWaveProgressBar.Size = new System.Drawing.Size(194, 23);
            this.generateWaveProgressBar.TabIndex = 2;
            // 
            // generateWaveProgressLabel
            // 
            this.generateWaveProgressLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.generateWaveProgressLabel.AutoSize = true;
            this.generateWaveProgressLabel.Location = new System.Drawing.Point(212, 461);
            this.generateWaveProgressLabel.Name = "generateWaveProgressLabel";
            this.generateWaveProgressLabel.Size = new System.Drawing.Size(0, 13);
            this.generateWaveProgressLabel.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 488);
            this.Controls.Add(this.generateWaveProgressLabel);
            this.Controls.Add(this.generateWaveProgressBar);
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.mainMenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.mainMenuStrip;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Custom Level Editor";
            this.mainTabControl.ResumeLayout(false);
            this.levelTabPage.ResumeLayout(false);
            this.gameFeaturesToUnlockLabel.ResumeLayout(false);
            this.gameFeaturesToUnlockLabel.PerformLayout();
            this.musicTypeGroupBox.ResumeLayout(false);
            this.musicTypeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.levelNumBox)).EndInit();
            this.waveTabPage.ResumeLayout(false);
            this.waveTabPanel.ResumeLayout(false);
            this.waveTabPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waveSpendingPointsIncrementNumBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveSpendingPointsNumBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxNextWaveHealthNumBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minNextWaveHealthNumBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveNumBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flagNumBox)).EndInit();
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage levelTabPage;
        private System.Windows.Forms.TabPage lawnTabPage;
        private System.Windows.Forms.TabPage waveTabPage;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.Panel gameFeaturesToUnlockLabel;
        private System.Windows.Forms.Label levelNameLabel;
        private System.Windows.Forms.CheckBox levelNameDefaultCheckBox;
        private System.Windows.Forms.Label levelWorldTypeLabel;
        private System.Windows.Forms.CheckBox levelDescDefaultCheckBox;
        private System.Windows.Forms.TextBox levelDescTextBox;
        private System.Windows.Forms.Label levelDescLabel;
        private System.Windows.Forms.ComboBox levelWorldTypeComboBox;
        private System.Windows.Forms.TextBox levelNameTextBox;
        private System.Windows.Forms.Label levelNumLabel;
        private System.Windows.Forms.NumericUpDown levelNumBox;
        private System.Windows.Forms.Label commentLabel;
        private System.Windows.Forms.TextBox commentTextBox;
        private System.Windows.Forms.CheckBox mowerCheckBox;
        private System.Windows.Forms.CheckBox sunDropCheckBox;
        private System.Windows.Forms.Label firstIntroNarrativeLabel;
        private System.Windows.Forms.Label firstOutroNarrativeLabel;
        private System.Windows.Forms.Label firstRewardParamLabel;
        private System.Windows.Forms.Label firstRewardTypeLabel;
        private System.Windows.Forms.CheckBox forceToWorldMapCheckBox;
        private System.Windows.Forms.ComboBox firstRewardTypeComboBox;
        private System.Windows.Forms.ComboBox firstRewardParamComboBox;
        private System.Windows.Forms.GroupBox musicTypeGroupBox;
        private System.Windows.Forms.RadioButton musicDefaultRadioButton;
        private System.Windows.Forms.RadioButton musicBRadioButton;
        private System.Windows.Forms.RadioButton musicARadioButton;
        private System.Windows.Forms.Button seedBankPropButton;
        private System.Windows.Forms.Label seedBankPropLabel;
        private System.Windows.Forms.Label miniGamePropLabel;
        private System.Windows.Forms.Button miniGamePropButton;
        private System.Windows.Forms.Panel waveTabPanel;
        private System.Windows.Forms.ComboBox firstIntroNarrativeComboBox;
        private System.Windows.Forms.ComboBox firstOutroNarrativeComboBox;
        private System.Windows.Forms.CheckBox tutorialPeashooterDeathCheckBox;
        private System.Windows.Forms.CheckBox supressDynamicTutorialCheckBox;
        private System.Windows.Forms.CheckBox repeatPlayForceToWorldMapCheckBox;
        private System.Windows.Forms.NumericUpDown flagNumBox;
        private System.Windows.Forms.Label numFlagLabel;
        private System.Windows.Forms.NumericUpDown maxNextWaveHealthNumBox;
        private System.Windows.Forms.Label maxNextWaveHealthLabel;
        private System.Windows.Forms.NumericUpDown minNextWaveHealthNumBox;
        private System.Windows.Forms.Label minNextWaveHealthLabel;
        private System.Windows.Forms.NumericUpDown waveNumBox;
        private System.Windows.Forms.Label numWaveLabel;
        private System.Windows.Forms.NumericUpDown waveSpendingPointsIncrementNumBox;
        private System.Windows.Forms.Label waveSpendingPointsIncrementLabel;
        private System.Windows.Forms.NumericUpDown waveSpendingPointsNumBox;
        private System.Windows.Forms.Label waveSpendingPointsLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label dynamicWavePropLabel;
        private System.Windows.Forms.Button generateWaveButton;
        private System.Windows.Forms.Panel wavePanel;
        private System.ComponentModel.BackgroundWorker generateWaveWorker;
        private System.Windows.Forms.ProgressBar generateWaveProgressBar;
        private System.Windows.Forms.Label generateWaveProgressLabel;
        private System.Windows.Forms.ListBox gameFeaturesToUnlockListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox tutorialIntroComboBox;
        private System.Windows.Forms.Label tutorialIntroLabel;
        private System.Windows.Forms.ToolStripMenuItem advancedModeToolStripMenuItem;

    }
}