﻿using Newtonsoft.Json;
using PvZ2CustomLevelEditor.Enums;
using PvZ2CustomLevelEditor.Forms.Components;
using PvZ2CustomLevelEditor.Helpers;
using PvZ2CustomLevelEditor.Models;
using PvZ2CustomLevelEditor.Models.Tutorials;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvZ2CustomLevelEditor.Forms
{
    public partial class MainForm : Form
    {
        private TableLayoutPanel waveTable;
        private MiniGameForm MiniGameForm { get; set; }
        private SeedBankForm SeedBankForm { get; set; }
        private List<ZombieWaveForm> ZombieWaveForms { get; set; }
        private CustomLevel CustomLevel { get; set; }
        public MainForm()
        {
            InitializeComponent();
            this.MiniGameForm = new MiniGameForm();
            this.SeedBankForm = new SeedBankForm();
            this.Text = String.Format("PvZ2 Custom Level Editor v.{0}", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);

            this.SetWorldTypeValues();
            this.SetFirstRewardTypeValues();
            this.SetTextBoxValues();
            this.SetFirstLevelNarrative();
            this.SetTutorialIntroValues();
            this.SetGameFeaturesToUnlockListBoxValues();
            this.SetAdvancedOptions(false);
        }

        #region initializers
        private void SetTextBoxValues()
        {
            levelNameTextBox.Text = StringHelper.GetWorldLevelName(this.GetSelectedLevelWorldType());
            levelNameTextBox.Enabled = false;
            levelDescTextBox.Text = StringHelper.GetWorldTripDescription(this.GetSelectedLevelWorldType());
            levelDescTextBox.Enabled = false;
        }

        private void SetWorldTypeValues()
        {
            var worldTypes = EnumUtil.GetValues<WorldType>();
            foreach (WorldType worldType in worldTypes)
            {
                ComboBoxItem item = new ComboBoxItem()
                {
                    Text = StringHelper.GetWorldTypeName(worldType),
                    Value = worldType
                };
                levelWorldTypeComboBox.Items.Add(item);
            }
            levelWorldTypeComboBox.SelectedIndex = 0;
        }

        private void SetFirstRewardTypeValues()
        {
            ComboBoxItem item = new ComboBoxItem()
            {
                Text = "None",
                Value = FirstRewardType.none
            };
            firstRewardTypeComboBox.Items.Add(item);
            item = new ComboBoxItem()
            {
                Text = "Gift Box",
                Value = FirstRewardType.giftbox
            };
            firstRewardTypeComboBox.Items.Add(item);
            item = new ComboBoxItem()
            {
                Text = "Unlock Plant",
                Value = FirstRewardType.unlock_plant
            };
            firstRewardTypeComboBox.Items.Add(item);
            item = new ComboBoxItem()
            {
                Text = "World Trophy",
                Value = FirstRewardType.worldtrophy
            };
            firstRewardTypeComboBox.Items.Add(item);
            item = new ComboBoxItem()
            {
                Text = "World Map",
                Value = FirstRewardType.mapgadget
            };
            firstRewardTypeComboBox.Items.Add(item);
            firstRewardTypeComboBox.SelectedIndex = 0;
        }

        private void SetFirstRewardParamValues(FirstRewardType firstRewardType)
        {
            firstRewardParamComboBox.Items.Clear();
            switch (firstRewardType)
            {
                case FirstRewardType.unlock_plant:
                    var plantList = EnumUtil.GetValues<PlantType>();
                    foreach (PlantType plant in plantList)
                    {
                        ComboBoxItem item = new ComboBoxItem()
                        {
                            Text = StringHelper.GetPlantName(plant),
                            Value = plant
                        };
                        firstRewardParamComboBox.Items.Add(item);
                    }
                    firstRewardParamComboBox.SelectedIndex = 0;
                    break;
                case FirstRewardType.worldtrophy:
                    var worldTypes = EnumUtil.GetValues<WorldType>();
                    foreach (WorldType worldType in worldTypes)
                    {
                        ComboBoxItem item = new ComboBoxItem()
                        {
                            Text = StringHelper.GetWorldTypeName(worldType) + " Endless Zone",
                            Value = "dangerroom_" + EnumUtil.ConvertToString(worldType).ToLower()
                        };
                        firstRewardParamComboBox.Items.Add(item);
                    }
                    firstRewardParamComboBox.SelectedIndex = 0;
                    break;
            }
        }

        private void SetFirstLevelNarrative()
        {
            firstIntroNarrativeComboBox.Items.AddRange(StringHelper.FirstIntroNarrative.ToArray());
            firstIntroNarrativeComboBox.SelectedIndex = 0;
            firstOutroNarrativeComboBox.Items.AddRange(StringHelper.FirstOutroNarrative.ToArray());
            firstOutroNarrativeComboBox.SelectedIndex = 0;
        }

        private void SetGameFeaturesToUnlockListBoxValues()
        {
            var features = EnumUtil.GetValues<UnlockableGameFeature>();
            foreach (UnlockableGameFeature feature in features)
            {
                gameFeaturesToUnlockListBox.Items.Add(feature);
            }
        }

        private void SetTutorialIntroValues()
        {
            var tutorialIntros = EnumUtil.GetValues<TutorialIntroType>();
            foreach (TutorialIntroType intro in tutorialIntros)
            {
                tutorialIntroComboBox.Items.Add(intro);
            }
            tutorialIntroComboBox.SelectedIndex = 0;
        }
        #endregion

        #region methods
        private WorldType GetSelectedLevelWorldType()
        {
            return (WorldType)(levelWorldTypeComboBox.SelectedItem as ComboBoxItem).Value;
        }

        private FirstRewardType GetSelectedFirstRewardType()
        {
            return (FirstRewardType)(firstRewardTypeComboBox.SelectedItem as ComboBoxItem).Value;
        }

        private object GetSelectedFirstRewardParam()
        {
            return (firstRewardParamComboBox.SelectedItem as ComboBoxItem).Value;
        }

        private List<string> SetInitialLevelModules()
        {
            List<string> modules = new List<string>()
            {
                "DefaultZombieWinCondition@LevelModules",
                "ZombiesDeadWinCon@LevelModules",
            };
            if ((TutorialIntroType)tutorialIntroComboBox.SelectedItem == TutorialIntroType.None)
            {
                modules.Add("StandardIntro@LevelModules");
            }
            if (mowerCheckBox.Checked)
            {
                modules.Add(StringHelper.GetMowerName(this.GetSelectedLevelWorldType()));
            }
            if (sunDropCheckBox.Checked)
            {
                modules.Add("DefaultSunDropper@LevelModules");
            }
            if (tutorialPeashooterDeathCheckBox.Checked)
            {
                modules.Add("TutorialPeashooterDeath@LevelModules");
            }

            return modules;
        }

        private void SetAdvancedOptions(bool value)
        {
            advancedModeToolStripMenuItem.Checked = value;
            firstIntroNarrativeComboBox.Enabled = value;
            firstOutroNarrativeComboBox.Enabled = value;
            firstRewardTypeComboBox.Enabled = value;
            firstRewardParamComboBox.Enabled = value;
            gameFeaturesToUnlockListBox.Enabled = value;
            musicTypeGroupBox.Enabled = value;
            tutorialIntroComboBox.Enabled = value;
            forceToWorldMapCheckBox.Enabled = value;
            tutorialPeashooterDeathCheckBox.Enabled = value;
            repeatPlayForceToWorldMapCheckBox.Enabled = value;
            supressDynamicTutorialCheckBox.Enabled = value;
        }

        private void GenerateCustomLevelProperties()
        {
            this.CustomLevel = new CustomLevel();
            this.CustomLevel.Comment = commentTextBox.Text;
            ObjectClass objectClass;

            #region level definition
            objectClass = new ObjectClass();
            LevelDefinition levelDefinition = new LevelDefinition(this.GetSelectedLevelWorldType());
            levelDefinition.Name = levelNameTextBox.Text;
            levelDefinition.Description = levelDescTextBox.Text;
            levelDefinition.LevelNumber = (int)levelNumBox.Value;
            levelDefinition.Modules = this.SetInitialLevelModules();
            levelDefinition.StageModule = StringHelper.GetWorldStageModule(this.GetSelectedLevelWorldType());
            levelDefinition.ForceToWorldMap = forceToWorldMapCheckBox.Checked ? true : (bool?)null;
            if (musicDefaultRadioButton.Checked)
            {
                levelDefinition.MusicType = null;
            }
            else if (musicARadioButton.Checked)
            {
                levelDefinition.MusicType = "MiniGame_A";
            }
            else if (musicBRadioButton.Checked)
            {
                levelDefinition.MusicType = "MiniGame_B";
            }
            if (this.GetSelectedFirstRewardType() != FirstRewardType.none)
            {
                levelDefinition.FirstRewardType = this.GetSelectedFirstRewardType();
            }
            if (firstRewardParamComboBox.SelectedIndex != -1)
            {
                object firstRewardParam = this.GetSelectedFirstRewardParam();
                if (firstRewardParam is PlantType) {
                    levelDefinition.FirstRewardParam = EnumUtil.ConvertToString((PlantType)firstRewardParam);
                }
                else if (firstRewardParam is String)
                {
                    levelDefinition.FirstRewardParam = firstRewardParam.ToString();
                }
            }
            levelDefinition.RepeatPlayForceToWorldMap = repeatPlayForceToWorldMapCheckBox.Checked ? true : (bool?)null;
            levelDefinition.SuppressDynamicTutorial = supressDynamicTutorialCheckBox.Checked ? true : (bool?)null;
            levelDefinition.FirstIntroNarrative = !firstIntroNarrativeComboBox.Text.Equals("None") ? firstIntroNarrativeComboBox.Text : null;
            levelDefinition.FirstOutroNarrative = !firstOutroNarrativeComboBox.Text.Equals("None") ? firstOutroNarrativeComboBox.Text : null;
            foreach (UnlockableGameFeature item in gameFeaturesToUnlockListBox.SelectedItems)
            {
                if (levelDefinition.GameFeaturesToUnlock == null)
                {
                    levelDefinition.GameFeaturesToUnlock = new List<UnlockableGameFeature>();
                }
                levelDefinition.GameFeaturesToUnlock.Add(item);
            }
            objectClass.ObjData = levelDefinition;
            this.CustomLevel.Objects.Add(objectClass);
            #endregion

            #region tutorial intro
            if ((TutorialIntroType)tutorialIntroComboBox.SelectedItem != TutorialIntroType.None)
            {
                objectClass = new ObjectClass();
                BaseTutorialIntro tutorialIntro = null;
                TutorialIntroType selectedTutorialIntro = (TutorialIntroType)tutorialIntroComboBox.SelectedItem;
                switch (selectedTutorialIntro)
                {
                    case TutorialIntroType.PlantFoodTutorialIntro:
                        tutorialIntro = new PlantfoodTutorialIntroProperties();
                        break;
                    case TutorialIntroType.PowerTileTutorialIntro:
                        tutorialIntro = new PowerTileIntroProperties();
                        break;
                }
                if (tutorialIntro != null)
                {
                    objectClass.ObjData = tutorialIntro;
                    this.CustomLevel.Objects.Add(objectClass);
                }
            }
            #endregion

            #region sun bomb
            if (MiniGameForm.sunBombCheckBox.Checked)
            {
                objectClass = new ObjectClass();
                SunDropperProperties sunDropperProperties = new SunDropperProperties();
                objectClass.ObjData = sunDropperProperties;
                this.CustomLevel.Objects.Add(objectClass);
            }
            #endregion

            #region seed bank
            if (!MiniGameForm.conveyorBeltCheckBox.Checked)
            {
                objectClass = new ObjectClass();
                SeedBankProperties seedBankProp = new SeedBankProperties();
                if (SeedBankForm.chooserRadioButton.Checked)
                {
                    seedBankProp.SelectionMethod = SeedBankSelectionMethod.chooser;
                }
                else if (SeedBankForm.presetRadioButton.Checked)
                {
                    seedBankProp.SelectionMethod = SeedBankSelectionMethod.preset;
                }
                List<Button> buttons = SeedBankForm.presetPlantsLayoutPanel.Controls.OfType<Button>().ToList();
                if (buttons.Count > 0)
                {
                    seedBankProp.PresetPlantList = new List<PresetPlant>();
                    foreach (Button button in buttons)
                    {
                        PlantType plantType;
                        Enum.TryParse(button.Name.Split('-')[1], out plantType);
                        int level = 0;
                        Int32.TryParse(button.Name.Split('-')[2], out level);

                        seedBankProp.PresetPlantList.Add(new PresetPlant()
                        {
                            PlantType = plantType,
                            Level = level != 0 ? level : (int?)null
                        });
                    }
                }
                buttons = SeedBankForm.blacklistedPlantsLayoutPanel.Controls.OfType<Button>().ToList();
                if (buttons.Count > 0)
                {
                    foreach (Button button in buttons)
                    {
                        PlantType plantType;
                        Enum.TryParse(button.Name.Split('-')[1], out plantType);

                        seedBankProp.PlantBlackList.Add(plantType);
                    }
                }
                objectClass.ObjData = seedBankProp;
                this.CustomLevel.Objects.Add(objectClass);
            }
            #endregion

            if (this.ZombieWaveForms != null)
            {
                #region new waves
                objectClass = new ObjectClass();
                WaveManagerModuleProperties waveManagerModuleProperties = new WaveManagerModuleProperties();
                objectClass.ObjData = waveManagerModuleProperties;
                this.CustomLevel.Objects.Add(objectClass);
                #endregion

                #region wave manager props
                objectClass = new ObjectClass();
                WaveManagerProperties waveManagerProperties = new WaveManagerProperties();
                if (minNextWaveHealthNumBox.Value > 0 && maxNextWaveHealthNumBox.Value > 0)
                {
                    waveManagerProperties.MinNextWaveHealthPercentage = (float)minNextWaveHealthNumBox.Value / 100;
                    waveManagerProperties.MaxNextWaveHealthPercentage = (float)maxNextWaveHealthNumBox.Value / 100;
                }
                waveManagerProperties.FlagWaveInterval = (int)waveNumBox.Value;
                waveManagerProperties.WaveCount = (int)flagNumBox.Value * (int)waveNumBox.Value;
                waveManagerProperties.WaveSpendingPointIncrement = (int)waveSpendingPointsIncrementNumBox.Value;
                waveManagerProperties.WaveSpendingPoints = (int)waveSpendingPointsNumBox.Value;
                objectClass.ObjData = waveManagerProperties;
                this.CustomLevel.Objects.Add(objectClass);
                #endregion

                #region wave details
                int i = 1;
                foreach (ZombieWaveForm wave in this.ZombieWaveForms)
                {
                    objectClass = new ObjectClass();
                    SpawnZombiesJitteredWaveActionProps spawnProp = new SpawnZombiesJitteredWaveActionProps(i++);
                    spawnProp.Zombies = new List<Zombie>();
                    List<Button> buttons = wave.zombieInWaveLayoutPanel.Controls.OfType<Button>().ToList();
                    foreach (Button button in buttons)
                    {
                        ZombieType zombieType;
                        Enum.TryParse(button.Name.Split('-')[1], out zombieType);
                        int row;
                        Int32.TryParse(button.Name.Split('-')[2], out row);

                        Zombie zombie = new Zombie();
                        zombie.Type = zombieType;
                        if (row != 0)
                        {
                            zombie.Row = row;
                        }

                        spawnProp.Zombies.Add(zombie);
                    }
                    objectClass.ObjData = spawnProp;
                    this.CustomLevel.Objects.Add(objectClass);
                }
                #endregion
            }

        }
        #endregion

        private void exportToolStripMenuItem_onClick(object sender, EventArgs e)
        {
            this.GenerateCustomLevelProperties();
            this.CustomLevel.UpdateLevelDefinitionModules();
            string output = JsonConvert.SerializeObject(this.CustomLevel,
                            Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
            string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\WriteLines.json"))
            {
                outputFile.WriteLine(output);
            }

            MessageBox.Show("Exported successfully");
        }

        private void levelNameDefaultCheckBox_onCheckedChanged(object sender, EventArgs e)
        {
            if (levelNameDefaultCheckBox.Checked)
            {
                levelNameTextBox.Enabled = false;
                levelNameTextBox.Text = StringHelper.GetWorldLevelName(this.GetSelectedLevelWorldType());
            }
            else
            {
                levelNameTextBox.Enabled = true;
            }
        }

        private void levelDescDefaultCheckBox_onCheckedChanged(object sender, EventArgs e)
        {
            if (levelDescDefaultCheckBox.Checked)
            {
                levelDescTextBox.Enabled = false;
                levelDescTextBox.Text = StringHelper.GetWorldTripDescription(this.GetSelectedLevelWorldType());
            }
            else
            {
                levelDescTextBox.Enabled = true;
            }
        }

        private void miniGamePropButton_onClick(object sender, EventArgs e)
        {
            MiniGameForm.sunBombCheckBox.Enabled = sunDropCheckBox.Checked;
            MiniGameForm.ShowDialog();

            seedBankPropButton.Enabled = !MiniGameForm.conveyorBeltCheckBox.Checked;
        }

        private void firstRewardTypeComboBox_onSelectedIndexChanged(object sender, EventArgs e)
        {
            FirstRewardType firstRewardType = this.GetSelectedFirstRewardType();
            if (firstRewardType == FirstRewardType.none || firstRewardType == FirstRewardType.giftbox || firstRewardType == FirstRewardType.mapgadget)
            {
                firstRewardParamComboBox.Enabled = false;
            }
            else
            {
                firstRewardParamComboBox.Enabled = true;
            }
            this.SetFirstRewardParamValues(firstRewardType);
        }

        private void levelWorldTypeComboBox_onSelectedIndexChanged(object sender, EventArgs e)
        {
            if (levelNameDefaultCheckBox.Checked)
            {
                levelNameTextBox.Text = StringHelper.GetWorldLevelName(this.GetSelectedLevelWorldType());
            }
            if (levelDescDefaultCheckBox.Checked)
            {
                levelDescTextBox.Text = StringHelper.GetWorldTripDescription(this.GetSelectedLevelWorldType());
            }

            // Big Wave Beach only has 1 mini game music
            if (this.GetSelectedLevelWorldType() == WorldType.Beach)
            {
                musicBRadioButton.Enabled = false;
                if (musicBRadioButton.Checked)
                {
                    musicARadioButton.Checked = true;
                }
            }
            else
            {
                musicBRadioButton.Enabled = true;
            }
        }

        private void generateWaveButton_onClick(object sender, EventArgs e)
        {
            DialogResult result = DialogResult.Yes;
            if (this.waveTable != null)
            {
                result = MessageBox.Show("Pressing Yes will erase the existing wave settings. Are you sure?", "", MessageBoxButtons.YesNo);
            }

            if (result == DialogResult.Yes)
            {
                this.wavePanel.Controls.Remove(this.waveTable);
                generateWaveProgressBar.Value = 0;
                generateWaveProgressLabel.Text = "Generating Wave...";
                generateWaveButton.Enabled = false;
                mainMenuStrip.Enabled = false;
                generateWaveWorker.RunWorkerAsync();
            }
        }

        private void waveTablePropButton_onClick(object sender, EventArgs e)
        {
            int waveNum = Int32.Parse(((Control)(sender)).Name.Split('_')[1]);
            ZombieWaveForms[waveNum - 1].ShowDialog();

            StringBuilder builder = new StringBuilder();
            builder.Append("Wave ").Append(waveNum).AppendLine();
            builder.Append("Zombies: ").Append(ZombieWaveForms[waveNum - 1].zombieInWaveLayoutPanel.Controls.OfType<Button>().Count());

            waveTable.Controls.OfType<Label>().First(x => x.Name.Equals("waveTablePropLabel_" + waveNum)).Text = builder.ToString();
        }

        private void generateWaveWorker_doWork(object sender, DoWorkEventArgs e)
        {
            ZombieWaveForms = new List<ZombieWaveForm>();
            int totalWave = (int)flagNumBox.Value * (int)waveNumBox.Value;

            TableLayoutPanel waveTable = new TableLayoutPanel();
            waveTable.AutoSize = true;
            waveTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            waveTable.ColumnCount = 2;
            waveTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            waveTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            waveTable.Location = new System.Drawing.Point(0, 0);
            waveTable.Name = "waveTable";
            waveTable.RowCount = totalWave;
            waveTable.Size = new System.Drawing.Size(589, 83);
            waveTable.TabIndex = 0;

            for (int i = 1; i <= totalWave; i++)
            {
                waveTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));

                Label label = new Label();
                label.Name = "waveTablePropLabel_" + i;
                label.Text = String.Format("Wave {0}", i);
                label.Dock = DockStyle.Fill;
                waveTable.Controls.Add(label);

                ZombieWaveForms.Add(new ZombieWaveForm());
                Button button = new Button();
                button.Name = "waveTablePropButton_" + i;
                button.Text = "Properties";
                button.Dock = DockStyle.Fill;
                button.Click += new System.EventHandler(this.waveTablePropButton_onClick);
                waveTable.Controls.Add(button);

                generateWaveWorker.ReportProgress((i * 100) / totalWave);
            }

            this.waveTable = waveTable;
        }

        private void generateWaveWorker_onProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            generateWaveProgressBar.Value = e.ProgressPercentage;
        }

        private void generateWaveWorker_onRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.wavePanel.Controls.Add(this.waveTable);
            generateWaveProgressLabel.Text = "Wave Generated";
            generateWaveButton.Enabled = true;
            mainMenuStrip.Enabled = true;
        }

        private void seedBankPropButton_onClick(object sender, EventArgs e)
        {
            SeedBankForm.ShowDialog();
        }

        private void panel_onMouseEnter(object sender, EventArgs e)
        {
            ((Control)(sender)).Focus();
        }

        private void advancedModeToolStripMenuItem_onClick(object sender, EventArgs e)
        {
            bool value = !advancedModeToolStripMenuItem.Checked;
            this.SetAdvancedOptions(value);
        }
    }
}
