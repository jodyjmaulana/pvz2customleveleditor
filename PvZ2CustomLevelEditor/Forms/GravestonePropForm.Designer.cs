﻿namespace PvZ2CustomLevelEditor.Forms
{
    partial class GravestonePropForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lawnGridPanelDummy = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // lawnGridPanelDummy
            // 
            this.lawnGridPanelDummy.Location = new System.Drawing.Point(12, 12);
            this.lawnGridPanelDummy.Name = "lawnGridPanelDummy";
            this.lawnGridPanelDummy.Size = new System.Drawing.Size(485, 220);
            this.lawnGridPanelDummy.TabIndex = 0;
            // 
            // GravestonePropForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 363);
            this.Controls.Add(this.lawnGridPanelDummy);
            this.Name = "GravestonePropForm";
            this.Text = "GravestonePropForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel lawnGridPanelDummy;
    }
}