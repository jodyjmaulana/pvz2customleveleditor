﻿namespace PvZ2CustomLevelEditor.Forms
{
    partial class MiniGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sunBombCheckBox = new System.Windows.Forms.CheckBox();
            this.acceptButton = new System.Windows.Forms.Button();
            this.sunBombGroupBox = new System.Windows.Forms.GroupBox();
            this.conveyorBeltGroupBox = new System.Windows.Forms.GroupBox();
            this.conveyorBeltCheckBox = new System.Windows.Forms.CheckBox();
            this.sunBombGroupBox.SuspendLayout();
            this.conveyorBeltGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // sunBombCheckBox
            // 
            this.sunBombCheckBox.AutoSize = true;
            this.sunBombCheckBox.Location = new System.Drawing.Point(6, 19);
            this.sunBombCheckBox.Name = "sunBombCheckBox";
            this.sunBombCheckBox.Size = new System.Drawing.Size(59, 17);
            this.sunBombCheckBox.TabIndex = 0;
            this.sunBombCheckBox.Text = "Enable";
            this.sunBombCheckBox.UseVisualStyleBackColor = true;
            // 
            // acceptButton
            // 
            this.acceptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.acceptButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.acceptButton.Location = new System.Drawing.Point(397, 314);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 2;
            this.acceptButton.Text = "OK";
            this.acceptButton.UseVisualStyleBackColor = true;
            // 
            // sunBombGroupBox
            // 
            this.sunBombGroupBox.Controls.Add(this.sunBombCheckBox);
            this.sunBombGroupBox.Location = new System.Drawing.Point(12, 12);
            this.sunBombGroupBox.Name = "sunBombGroupBox";
            this.sunBombGroupBox.Size = new System.Drawing.Size(214, 44);
            this.sunBombGroupBox.TabIndex = 4;
            this.sunBombGroupBox.TabStop = false;
            this.sunBombGroupBox.Text = "Sun Bomb";
            // 
            // conveyorBeltGroupBox
            // 
            this.conveyorBeltGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.conveyorBeltGroupBox.Controls.Add(this.conveyorBeltCheckBox);
            this.conveyorBeltGroupBox.Location = new System.Drawing.Point(232, 12);
            this.conveyorBeltGroupBox.Name = "conveyorBeltGroupBox";
            this.conveyorBeltGroupBox.Size = new System.Drawing.Size(240, 44);
            this.conveyorBeltGroupBox.TabIndex = 5;
            this.conveyorBeltGroupBox.TabStop = false;
            this.conveyorBeltGroupBox.Text = "Special Delivery";
            // 
            // conveyorBeltCheckBox
            // 
            this.conveyorBeltCheckBox.AutoSize = true;
            this.conveyorBeltCheckBox.Location = new System.Drawing.Point(6, 19);
            this.conveyorBeltCheckBox.Name = "conveyorBeltCheckBox";
            this.conveyorBeltCheckBox.Size = new System.Drawing.Size(59, 17);
            this.conveyorBeltCheckBox.TabIndex = 1;
            this.conveyorBeltCheckBox.Text = "Enable";
            this.conveyorBeltCheckBox.UseVisualStyleBackColor = true;
            // 
            // MiniGameForm
            // 
            this.AcceptButton = this.acceptButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 349);
            this.ControlBox = false;
            this.Controls.Add(this.conveyorBeltGroupBox);
            this.Controls.Add(this.sunBombGroupBox);
            this.Controls.Add(this.acceptButton);
            this.Name = "MiniGameForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Mini Game Properties";
            this.Load += new System.EventHandler(this.MiniGameForm_onLoad);
            this.sunBombGroupBox.ResumeLayout(false);
            this.sunBombGroupBox.PerformLayout();
            this.conveyorBeltGroupBox.ResumeLayout(false);
            this.conveyorBeltGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button acceptButton;
        public System.Windows.Forms.CheckBox sunBombCheckBox;
        private System.Windows.Forms.GroupBox sunBombGroupBox;
        private System.Windows.Forms.GroupBox conveyorBeltGroupBox;
        public System.Windows.Forms.CheckBox conveyorBeltCheckBox;
    }
}