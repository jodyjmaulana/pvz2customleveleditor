﻿using System;

namespace PvZ2CustomLevelEditor.Enums
{
    public enum TutorialIntroType
    {
        None,
        PlantFoodTutorialIntro,
        PowerTileTutorialIntro
    }
}
