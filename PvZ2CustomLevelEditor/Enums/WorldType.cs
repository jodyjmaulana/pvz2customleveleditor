﻿using System;

namespace PvZ2CustomLevelEditor.Enums
{
    public enum WorldType
    {
        Egypt,
        Pirate,
        Cowboy,
        Future,
        Dark,
        Beach,
        Iceage,
        LostCity,
        Eighties,
        Dino,
        Modern
    }
}
