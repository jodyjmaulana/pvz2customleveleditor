﻿using System;

namespace PvZ2CustomLevelEditor.Enums
{
    public enum ZombieType
    {
        #region tutorial
        tutorial,
        tutorial_armor1,
        tutorial_armor2,
        tutorial_flag,
        tutorial_imp,
        tutorial_gargantuar,
        vase_gargantuar,
        treasureyeti,
        #endregion

        #region ancient egypt
        mummy,
        mummy_armor1,
        mummy_armor2,
        mummy_flag,
        ra,
        camel_almanac,
        camel_onehump,
        camel_twohump,
        camel_manyhump,
        camel_onehump_touch,
        camel_twohump_touch,
        explorer,
        tomb_raiser,
        pharaoh,
        egypt_imp,
        egypt_gargantuar,
        zombossmech_egypt,
        mummy_flag_veteran,
        mummy_armor4,
        explorer_veteran,
        #endregion

        #region pirate seas
        pirate,
        pirate_armor1,
        pirate_armor2,
        pirate_flag,
        swashbuckler,
        seagull,
        barrelroller,
        pirate_imp,
        cannon,
        pirate_captain,
        pirate_captain_parrot,
        pirate_gargantuar,
        zombossmech_pirate,
        pirate_armor4,
        pirate_flag_veteran,
        pelican,
        #endregion

        #region wild west
        cowboy,
        cowboy_armor1,
        cowboy_armor2,
        cowboy_flag,
        prospector,
        piano,
        poncho,
        poncho_plate,
        poncho_no_plate,
        chicken_farmer,
        chicken,
        west_bull,
        west_bullrider,
        cowboy_gargantuar,
        zombossmech_cowboy,
        #endregion

        #region far future
        future,
        future_armor1,
        future_armor2,
        future_flag,
        future_jetpack,
        future_protector,
        future_imp,
        mech_cone,
        disco_mech,
        future_jetpack_disco,
        football_mech,
        future_gargantuar,
        zombossmech_future,
        future_armor4,
        future_flag_veteran,
        future_jetpack_veteran,
        #endregion

        #region dark ages
        dark,
        dark_armor1,
        dark_armor2,
        dark_armor3,
        dark_flag,
        dark_imp,
        dark_juggler,
        dark_gargantuar,
        dark_wizard,
        dark_king,
        dark_imp_dragon,
        zombossmech_dark,
        #endregion

        #region big wave beach
        beach,
        beach_armor1,
        beach_armor2,
        beach_flag,
        beach_fem,
        beach_fem_armor1,
        beach_fem_armor2,
        beach_snorkel,
        beach_surfer,
        beach_imp,
        beach_gargantuar,
        beach_fisherman,
        beach_octopus,
        zombossmech_beach,
        #endregion

        #region frostbite caves
        iceage,
        iceage_armor1,
        iceage_armor2,
        iceage_armor3,
        iceage_flag,
        iceage_imp,
        iceage_gargantuar,
        iceage_hunter,
        iceage_dodo,
        iceage_weaselhoarder,
        iceage_weasel,
        iceage_troglobite,
        iceage_troglobite_2block,
        iceage_troglobite_1block,
        zombossmech_iceage,
        #endregion

        #region lost city
        lostcity,
        lostcity_armor1,
        lostcity_armor2,
        lostcity_flag,
        lostcity_excavator,
        lostcity_jane,
        lostcity_lostpilot,
        lostcity_bug,
        lostcity_bug_armor1,
        lostcity_bug_armor2,
        lostcity_impporter,
        lostcity_crystalskull,
        lostcity_relichunter,
        lostcity_imp,
        lostcity_gargantuar,
        zombossmech_lostcity,
        #endregion

        #region neon mixtape tour
        eighties,
        eighties_armor1,
        eighties_armor2,
        eighties_flag,
        eighties_gargantuar,
        eighties_imp,
        eighties_punk,
        eighties_mc,
        eighties_glitter,
        eighties_breakdancer,
        eighties_arcade,
        eighties_8bit,
        eighties_8bit_armor1,
        eighties_8bit_armor2,
        eighties_boombox,
        zombossmech_eighties,
        #endregion

        #region jurassic marsh
        dino,
        dino_armor1,
        dino_armor2,
        dino_armor3,
        dino_flag,
        dino_imp,
        dino_gargantuar,
        dino_bully,
        zombossmech_dino,
        dino_armor4,
        dino_flag_veteran,
        dino_bully_veteran,
        #endregion

        #region modern day
        modern_newspaper,
        modern_balloon,
        modern_allstar,
        modern_superfanimp,
        tutorial_armor4,
        modern_newspaper_veteran,
        tutorial_flag_veteran,
        #endregion

        #region others
        #region beghouled
        beghouled,
        beghouled_armor1,
        beghouled_armor2,
        beghouled_newspaper,
        #endregion

        #region springening
        spring,
        spring_armor1,
        spring_armor2,
        spring_flag,
        spring_poncho,
        spring_camel_onehump,
        spring_camel_twohump,
        spring_camel_manyhump,
        spring_wizard,
        spring_imp,
        spring_imp_af,
        spring_gargantuar,
        spring_gargantuar_af,
        #endregion

        #region st. paddy's day
        stpatrick,
        stpatrick_armor1,
        stpatrick_armor2,
        leprachaun_imp,
        leprachaun_dodo,
        #endregion

        #region heroes event
        hero_impfinity,
        hero_rustbolt,
        hero_superbrainz,
        hero_electricboogaloo,
        hero_smash,
        #endregion

        #region summer nights
        summer_basic,
        summer_armor1,
        summer_armor2,
        summer_flag,
        summer_imp,
        summer_gargantuar,
        summer_bug,
        summer_bug_armor1,
        summer_bug_armor2,
        #endregion

        #region valenbrainz
        valentines,
        valentines_armor1,
        valentines_armor2,
        valentines_flag,
        valentines_imp,
        valentines_gargantuar,
        #endregion

        #region lawn of doom
        halloween,
        halloween_armor1,
        halloween_armor2,
        halloween_flag,
        halloween_imp,
        halloween_gargantuar,
        #endregion

        #region birthdayz
        birthday,
        birthday_flag,
        birthday_gargantuar,
        birthday_juggler,
        birthday_pharaoh,
        birthday_jetpack,
        birthday_barrelroller,
        birthday_troglobite,
        #endregion

        #region unused
        pet,
        cleopatra,
        holiday_gargantuar,
        holiday_imp
        #endregion
        #endregion
    }
}
