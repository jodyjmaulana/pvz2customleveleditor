﻿using System;

namespace PvZ2CustomLevelEditor.Enums
{
    public enum UnlockableGameFeature
    {
        feature_worldmap,
        feature_plantfood,
        feature_almanac,
        feature_plantfood_purchase
    }
}
