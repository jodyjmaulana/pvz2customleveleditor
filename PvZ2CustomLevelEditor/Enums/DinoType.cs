﻿using System;

namespace PvZ2CustomLevelEditor.Enums
{
    public enum DinoType
    {
        raptor,
        stego,
        ptero,
        tyranno,
        ankylo
    }
}
