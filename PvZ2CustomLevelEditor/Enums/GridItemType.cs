﻿using System;

namespace PvZ2CustomLevelEditor.Enums
{
    public enum GridItemType
    {
        Gravestones,
        SliderTiles,
        GoldTiles
    }
}
