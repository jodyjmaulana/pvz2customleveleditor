﻿using System;

namespace PvZ2CustomLevelEditor.Enums
{
    public enum FirstRewardType
    {
        none,
        giftbox,
        unlock_plant,
        worldtrophy,
        mapgadget
    }
}
