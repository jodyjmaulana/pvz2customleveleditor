﻿using System;

namespace PvZ2CustomLevelEditor.Enums
{
    public enum TileType
    {
        slider_up,
        slider_down,
        goldtile
    }
}
