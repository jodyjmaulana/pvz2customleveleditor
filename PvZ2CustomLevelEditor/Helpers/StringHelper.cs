﻿using PvZ2CustomLevelEditor.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Helpers
{
    public static class StringHelper
    {
        public static List<string> FirstIntroNarrative
        {
            get
            {
                return new List<string>()
                {
                    "None",
                    "EGYPT_INTRO_FIRSTLEVEL"
                };
            }
        }

        public static List<string> FirstOutroNarrative
        {
            get
            {
                return new List<string>()
                {
                    "None",
                };
            }
        }

        public static string GetWorldTypeName(WorldType world)
        {
            string result = string.Empty;
            switch (world)
            {
                case WorldType.Egypt:
                    result = "Ancient Egypt";
                    break;
                case WorldType.Pirate:
                    result = "Pirate Seas";
                    break;
                case WorldType.Cowboy:
                    result = "Wild West";
                    break;
                case WorldType.Future:
                    result = "Far Future";
                    break;
                case WorldType.Dark:
                    result = "Dark Ages";
                    break;
                case WorldType.Beach:
                    result = "Big Wave Beach";
                    break;
                case WorldType.Iceage:
                    result = "Frostbite Caves";
                    break;
                case WorldType.LostCity:
                    result = "Lost City";
                    break;
                case WorldType.Eighties:
                    result = "Neon Mixtape Tour";
                    break;
                case WorldType.Dino:
                    result = "Jurassic Marsh";
                    break;
                case WorldType.Modern:
                    result = "Modern Day";
                    break;
            }
            return result;
        }

        public static string GetWorldTripDescription(WorldType world)
        {
            string result = String.Empty;
            switch (world)
            {
                case WorldType.Egypt:
                    result = "[PLAYERS_TRIP_TO_EGYPT]";
                    break;
                case WorldType.Pirate:
                    result = "[PLAYERS_TRIP_TO_PIRATE_SHIP]";
                    break;
                case WorldType.Cowboy:
                    result = "[PLAYERS_TRIP_TO_WEST]";
                    break;
                case WorldType.Future:
                    result = "[PLAYERS_TRIP_TO_FUTURE]";
                    break;
                case WorldType.Dark:
                    result = "[PLAYERS_TRIP_TO_DARKAGES]";
                    break;
                case WorldType.Beach:
                    result = "[PLAYERS_TRIP_TO_BEACH]";
                    break;
                case WorldType.Iceage:
                    result = "[PLAYERS_TRIP_TO_ICEAGE]";
                    break;
                case WorldType.LostCity:
                    result = "[PLAYERS_TRIP_TO_LOSTCITY]";
                    break;
                case WorldType.Eighties:
                    result = "[PLAYERS_TRIP_TO_EIGHTIES]";
                    break;
                case WorldType.Dino:
                    result = "[PLAYERS_TRIP_TO_DINO]";
                    break;
                case WorldType.Modern:
                    result = "[PLAYERS_TRIP_TO_MODERN]";
                    break;
            }
            return result;
        }

        public static string GetWorldLevelName(WorldType world)
        {
            string result = String.Empty;
            switch (world)
            {
                case WorldType.Egypt:
                    result = "[EGYPT_LEVEL_NAME]";
                    break;
                case WorldType.Pirate:
                    result = "[PIRATE_LEVEL_NAME]";
                    break;
                case WorldType.Cowboy:
                    result = "[WEST_LEVEL_NAME]";
                    break;
                case WorldType.Future:
                    result = "[FUTURE_LEVEL_NAME]";
                    break;
                case WorldType.Dark:
                    result = "[DARKAGES_LEVEL_NAME]";
                    break;
                case WorldType.Beach:
                    result = "[BEACH_LEVEL_NAME]";
                    break;
                case WorldType.Iceage:
                    result = "[ICEAGE_LEVEL_NAME]";
                    break;
                case WorldType.LostCity:
                    result = "[LOSTCITY_LEVEL_NAME]";
                    break;
                case WorldType.Eighties:
                    result = "[EIGHTIES_LEVEL_NAME]";
                    break;
                case WorldType.Dino:
                    result = "[DINO_LEVEL_NAME]";
                    break;
                case WorldType.Modern:
                    result = "[MODERN_LEVEL_NAME]";
                    break;
            }
            return result;
        }

        public static string GetWorldStageModule(WorldType world)
        {
            string result = String.Empty;
            result = EnumUtil.ConvertToString(world) + "Stage@LevelModules";
            switch (world)
            {
                case WorldType.Egypt:
                    result = "EgyptStage@LevelModules";
                    break;
                case WorldType.Pirate:
                    break;
                case WorldType.Cowboy:
                    break;
                case WorldType.Future:
                    result = "FutureStage@LevelModules";
                    break;
                case WorldType.Dark:
                    result = "DarkStage@LevelModules";
                    break;
                case WorldType.Beach:
                    break;
                case WorldType.Iceage:
                    result = "IceageStage@LevelModules";
                    break;
                case WorldType.LostCity:
                    result = "LostCityStage@LevelModules";
                    break;
                case WorldType.Eighties:
                    break;
                case WorldType.Dino:
                    break;
                case WorldType.Modern:
                    result = "ModernStage@LevelModules";
                    break;
            }
            return result;
        }

        public static string GetPresentTable(WorldType world, string presentTableType)
        {
            string result = String.Empty;
            result = EnumUtil.ConvertToString(world).ToLower() + "_" + presentTableType + "_01";
            switch (world)
            {
                case WorldType.Egypt:
                    result = "egypt_" + presentTableType + "_01";
                    break;
                case WorldType.Pirate:
                    break;
                case WorldType.Cowboy:
                    break;
                case WorldType.Future:
                    result = "future_" + presentTableType + "_01";
                    break;
                case WorldType.Dark:
                    result = "egypt_" + presentTableType + "_01";
                    break;
                case WorldType.Beach:
                    break;
                case WorldType.Iceage:
                    result = "egypt_" + presentTableType + "_01";
                    break;
                case WorldType.LostCity:
                    result = "lostcity_" + presentTableType + "_01";
                    break;
                case WorldType.Eighties:
                    break;
                case WorldType.Dino:
                    break;
                case WorldType.Modern:
                    result = "modern_" + presentTableType + "_01";
                    break;
            }
            return result;
        }

        public static string GetMowerName(WorldType world)
        {
            string result = String.Empty;
            result = EnumUtil.ConvertToString(world) + "Mowers@LevelModules";
            switch (world)
            {
                case WorldType.Egypt:
                    result = "EgyptMowers@LevelModules";
                    break;
                case WorldType.Pirate:
                    break;
                case WorldType.Cowboy:
                    break;
                case WorldType.Future:
                    result = "FutureMowers@LevelModules";
                    break;
                case WorldType.Dark:
                    result = "DarkMowers@LevelModules";
                    break;
                case WorldType.Beach:
                    break;
                case WorldType.Iceage:
                    result = "IceageMowers@LevelModules";
                    break;
                case WorldType.LostCity:
                    result = "LostCityMowers@LevelModules";
                    break;
                case WorldType.Eighties:
                    break;
                case WorldType.Dino:
                    break;
                case WorldType.Modern:
                    result = "ModernMowers@LevelModules";
                    break;
            }
            return result;
        }

        public static string GetZombieName(ZombieType zombie)
        {
            string result = string.Empty;

            return result;
        }

        public static string GetPlantName(PlantType plant)
        {
            string result = string.Empty;
            switch (plant)
            {
                case PlantType.sunflower:
                    result = "Sunflower";
                    break;
                case PlantType.peashooter:
                    result = "Peashooter";
                    break;
                case PlantType.wallnut:
                    result = "Wall-nut";
                    break;
                case PlantType.potatomine:
                    result = "Potato Mine";
                    break;
                case PlantType.bloomerang:
                    result = "Bloomerang";
                    break;
                case PlantType.cabbagepult:
                    result = "Cabbage-pult";
                    break;
                case PlantType.iceburg:
                    result = "Iceberg Lettuce";
                    break;
                case PlantType.gravebuster:
                    result = "Grave Buster";
                    break;
                case PlantType.twinsunflower:
                    result = "Twin Sunflower";
                    break;
                case PlantType.bonkchoy:
                    result = "Bonk Choy";
                    break;
                case PlantType.repeater:
                    result = "Repeater";
                    break;
                case PlantType.snowpea:
                    result = "Snow Pea";
                    break;
                case PlantType.kernelpult:
                    result = "Kernel-pult";
                    break;
                case PlantType.snapdragon:
                    result = "Snapdragon";
                    break;
                case PlantType.powerlily:
                    result = "Power Lily";
                    break;
                case PlantType.spikeweed:
                    result = "Spikeweed";
                    break;
                case PlantType.coconutcannon:
                    result = "Coconut Cannon";
                    break;
                case PlantType.cherry_bomb:
                    result = "Cherry Bomb";
                    break;
                case PlantType.springbean:
                    result = "Spring Bean";
                    break;
                case PlantType.spikerock:
                    result = "Spikerock";
                    break;
                case PlantType.threepeater:
                    result = "Threepeater";
                    break;
                case PlantType.squash:
                    result = "Squash";
                    break;
                case PlantType.splitpea:
                    result = "Split Pea";
                    break;
                case PlantType.chilibean:
                    result = "Chili Bean";
                    break;
                case PlantType.torchwood:
                    result = "Torchwood";
                    break;
                case PlantType.lightningreed:
                    result = "Lightning Reed";
                    break;
                case PlantType.tallnut:
                    result = "Tall-nut";
                    break;
                case PlantType.jalapeno:
                    result = "Jalapeno";
                    break;
                case PlantType.peapod:
                    result = "Pea Pod";
                    break;
                case PlantType.melonpult:
                    result = "Melon-pult";
                    break;
                case PlantType.wintermelon:
                    result = "Winter Melon";
                    break;
                case PlantType.imitater:
                    result = "Imitater";
                    break;
                case PlantType.marigold:
                    result = "Marigold";
                    break;
                case PlantType.laser_bean:
                    result = "Laser Bean";
                    break;
                case PlantType.blover:
                    result = "Blover";
                    break;
                case PlantType.citron:
                    result = "Citron";
                    break;
                case PlantType.empea:
                    result = "E.M.Peach";
                    break;
                case PlantType.starfruit:
                    result = "Starfruit";
                    break;
                case PlantType.holonut:
                    result = "Infi-nut";
                    break;
                case PlantType.magnifyinggrass:
                    result = "Magnifying Grass";
                    break;
                case PlantType.powerplant:
                    result = "Tile Turnip";
                    break;
                case PlantType.hypnoshroom:
                    result = "Hypno-shroom";
                    break;
                case PlantType.sunshroom:
                    result = "Sun-shroom";
                    break;
                case PlantType.puffshroom:
                    result = "Puff-shroom";
                    break;
                case PlantType.fumeshroom:
                    result = "Fume-shroom";
                    break;
                case PlantType.sunbean:
                    result = "Sun Bean";
                    break;
                case PlantType.magnetshroom:
                    result = "Magnet-shroom";
                    break;
                case PlantType.peanut:
                    result = "Pea-nut";
                    break;
                case PlantType.chomper:
                    result = "Chomper";
                    break;
                case PlantType.lilypad:
                    result = "Lily Pad";
                    break;
                case PlantType.tanglekelp:
                    result = "Tangle Kelp";
                    break;
                case PlantType.bowlingbulb:
                    result = "Bowling Bulb";
                    break;
                case PlantType.ghostpepper:
                    result = "Ghost Pepper";
                    break;
                case PlantType.homingthistle:
                    result = "Homing Thistle";
                    break;
                case PlantType.guacodile:
                    result = "Guacodile";
                    break;
                case PlantType.banana:
                    result = "Banana Launcher";
                    break;
                case PlantType.sweetpotato:
                    result = "Sweet Potato";
                    break;
                case PlantType.sapfling:
                    result = "Sap-fling";
                    break;
                case PlantType.hurrikale:
                    result = "Hurrikale";
                    break;
                case PlantType.hotpotato:
                    result = "Hot Potato";
                    break;
                case PlantType.pepperpult:
                    result = "Pepper-pult";
                    break;
                case PlantType.chardguard:
                    result = "Chard Guard";
                    break;
                case PlantType.firepeashooter:
                    result = "Fire Peashooter";
                    break;
                case PlantType.stunion:
                    result = "Stunion";
                    break;
                case PlantType.xshot:
                    result = "Rotobaga";
                    break;
                case PlantType.dandelion:
                    result = "Dandelion";
                    break;
                case PlantType.lavaguava:
                    result = "Lava Guava";
                    break;
                case PlantType.redstinger:
                    result = "Red Stinger";
                    break;
                case PlantType.akee:
                    result = "A.K.E.E.";
                    break;
                case PlantType.endurian:
                    result = "Endurian";
                    break;
                case PlantType.toadstool:
                    result = "Toadstool";
                    break;
                case PlantType.stallia:
                    result = "Stallia";
                    break;
                case PlantType.goldleaf:
                    result = "Gold Leaf";
                    break;
                case PlantType.strawburst:
                    result = "Strawburst";
                    break;
                case PlantType.cactus:
                    result = "Cactus";
                    break;
                case PlantType.phatbeet:
                    result = "Phat Beet";
                    break;
                case PlantType.celerystalker:
                    result = "Celery Stalker";
                    break;
                case PlantType.thymewarp:
                    result = "Thyme Warp";
                    break;
                case PlantType.electricblueberry:
                    result = "Electric Blueberry";
                    break;
                case PlantType.garlic:
                    result = "Garlic";
                    break;
                case PlantType.sporeshroom:
                    result = "Spore-shroom";
                    break;
                case PlantType.intensivecarrot:
                    result = "Intensive Carrot";
                    break;
                case PlantType.jackolantern:
                    result = "Jack O' Lantern";
                    break;
                case PlantType.grapeshot:
                    result = "Grapeshot";
                    break;
                case PlantType.primalpeashooter:
                    result = "Primal Peashooter";
                    break;
                case PlantType.primalwallnut:
                    result = "Primal Wall-nut";
                    break;
                case PlantType.perfumeshroom:
                    result = "Perfume-shroom";
                    break;
                case PlantType.coldsnapdragon:
                    result = "Cold Snapdragon";
                    break;
                case PlantType.primalsunflower:
                    result = "Primal Sunflower";
                    break;
                case PlantType.primalpotatomine:
                    result = "Primal Potato Mine";
                    break;
                case PlantType.shrinkingviolet:
                    result = "Shrinking Violet";
                    break;
                case PlantType.moonflower:
                    result = "Moonflower";
                    break;
                case PlantType.nightshade:
                    result = "Nightshade";
                    break;
                case PlantType.shadowshroom:
                    result = "Shadow-shroom";
                    break;
                case PlantType.bloominghearts:
                    result = "Blooming Hearts";
                    break;
                case PlantType.dusklobber:
                    result = "Dusk Lobber";
                    break;
                case PlantType.escaperoot:
                    result = "Escape Root";
                    break;
                case PlantType.grimrose:
                    result = "Grimrose";
                    break;
                case PlantType.goldbloom:
                    result = "Gold Bloom";
                    break;
                case PlantType.electriccurrant:
                    result = "Electric Currant";
                    break;
                case PlantType.aloe:
                    result = "Aloe";
                    break;
                case PlantType.wasabiwhip:
                    result = "Wasabi Whip";
                    break;
                case PlantType.explodeonut:
                    result = "Explode-o-nut";
                    break;
                case PlantType.kiwibeast:
                    result = "Kiwibeast";
                    break;
                case PlantType.bombegranate:
                    result = "Bombegranate";
                    break;
                case PlantType.applemortar:
                    result = "Apple Mortar";
                    break;
                case PlantType.witchhazel:
                    result = "Witch Hazel";
                    break;
                case PlantType.parsnip:
                    result = "Parsnip";
                    break;
                case PlantType.missiletoe:
                    result = "Missile Toe";
                    break;
                case PlantType.hotdate:
                    result = "Hot Date";
                    break;
                case PlantType.caulipower:
                    result = "Caulipower";
                    break;
                    break;
            }
            return result;
        }

        public static string Dummy(WorldType world)
        {
            string result = string.Empty;
            switch (world)
            {
                case WorldType.Egypt:
                    break;
                case WorldType.Pirate:
                    break;
                case WorldType.Cowboy:
                    break;
                case WorldType.Future:
                    break;
                case WorldType.Dark:
                    break;
                case WorldType.Beach:
                    break;
                case WorldType.Iceage:
                    break;
                case WorldType.LostCity:
                    break;
                case WorldType.Eighties:
                    break;
                case WorldType.Dino:
                    break;
                case WorldType.Modern:
                    break;
            }
            return result;
        }
    }
}
