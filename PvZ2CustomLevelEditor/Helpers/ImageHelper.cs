﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PvZ2CustomLevelEditor.Helpers
{
    public static class ImageHelper
    {
        public static Bitmap GetZombieThumbnail(Enum item)
        {
            return GetThumbnail("zombie_", item);
        }
        public static Bitmap GetPlantThumbnail(Enum item)
        {
            return GetThumbnail("plant_", item);
        }
        private static Bitmap GetThumbnail(string prefix, Enum item)
        {
            return (System.Drawing.Bitmap)global::PvZ2CustomLevelEditor.Properties.Resources.ResourceManager.GetObject(prefix + EnumUtil.ConvertToString(item), null);
        }
        public static Bitmap GetZombieThumbnail(string item)
        {
            return GetThumbnail("zombie_", item);
        }
        public static Bitmap GetPlantThumbnail(string item)
        {
            return GetThumbnail("plant_", item);
        }
        private static Bitmap GetThumbnail(string prefix, string item)
        {
            return (System.Drawing.Bitmap)global::PvZ2CustomLevelEditor.Properties.Resources.ResourceManager.GetObject(prefix + item, null);
        }
    }
}
